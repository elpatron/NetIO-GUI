@echo off
SET MYVERSION=1.0.8
SET SEVENZIP=%ProgramFiles%\7-Zip\7z.exe
SET ISCC=%ProgramFiles(x86)%\Inno Setup 6\iscc.exe
if %2=="Debug" goto debug
cd %1
del .\setup\Output\*.exe
del .\setup\Output\*.zip
"%SEVENZIP%" a -sfx7z.sfx .\setup\output\netio-gui_v%MYVERSION%_portable.exe @filelist.txt
"%SEVENZIP%" a .\setup\output\netio-gui_v%MYVERSION%_release.zip @filelist.txt
cd .\setup
"%ISCC%" /Q /F"netio-gui_v%MYVERSION%_setup" ".\setup.iss"
cd ..
copy .\Speedometer\README.txt .\setup\output\
goto end

:debug
echo "Debug build, nothing else to do.""

:end

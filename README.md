# Netio-GUI

![.Net](https://img.shields.io/badge/.NET-5C2D91?style=for-the-badge&logo=.net&logoColor=white)![Windows](https://img.shields.io/badge/Windows-0078D6?style=for-the-badge&logo=windows&logoColor=white)

[![Download NetIO-GUI](https://img.shields.io/sourceforge/dm/netiogui.svg)](https://sourceforge.net/projects/netiogui/files/latest/download)

## About

*Netio-GUI* measures network connection speeds at different *TCP* or *UDP* package sizes between two peers. Results are saved to a *SQLite* database so they can be easily compared to each other.



![image-20221102174645791](./assets/image-20221102174645791.png)

The project was formerly hosted at [Sourceforge](https://sourceforge.net/projects/netiogui/), which still holds the latest binaries. The .NET Source code is hosted at [Codeberg](https://codeberg.org/elpatron/NetIO-GUI) from November 2022 until now.

The project started in 2012 as a *GUI* for [netio.exe](https://github.com/kai-uwe-rommel/netio), a command line tool from Kai Uwe Rommel written in C.

## Download and Installation

Download the latest release from *Sourceforge*:

[![Download NetIO-GUI](https://a.fsdn.com/con/app/sf-download-button)](https://sourceforge.net/projects/netiogui/files/latest/download)

You have the choice between a portable version, which can be started from anywhere (e.g. from an USB flash drive) and a version which comes as *Innosetup* installation program.

## Usage

To measure the connection speed between two peers, you have to start *Netio-GUI* on side 🅰️ in *Server-Mode* and on side 🅱️ in *Client-Mode*. Enter the local IP address of side 🅰️ in the corresponding field on side 🅱️ and start the measurement. Results are saved on side 🅱️.

![image-20221104130730368](./assets/image-20221104130730368.png)


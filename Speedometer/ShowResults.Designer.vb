﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ShowResults
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ShowResults))
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.btn_ShowAll = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btn_LocalHostfilter = New System.Windows.Forms.Button()
        Me.btn_RemoteHostfilter = New System.Windows.Forms.Button()
        Me.cb_LocalIPs = New System.Windows.Forms.ComboBox()
        Me.cb_RemoteIPs = New System.Windows.Forms.ComboBox()
        Me.BindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.DateiToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ErgebnisseexportierenToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FensterschließenToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        Me.MenuStrip2 = New System.Windows.Forms.MenuStrip()
        Me.DateiToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExportAlsCSVDateiToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FensterschließenToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MenuStrip1.SuspendLayout()
        Me.MenuStrip2.SuspendLayout()
        Me.SuspendLayout()
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AllowUserToOrderColumns = True
        Me.DataGridView1.AllowUserToResizeRows = False
        Me.DataGridView1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(13, 27)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.Size = New System.Drawing.Size(1034, 237)
        Me.DataGridView1.TabIndex = 0
        '
        'btn_ShowAll
        '
        Me.btn_ShowAll.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btn_ShowAll.Location = New System.Drawing.Point(13, 271)
        Me.btn_ShowAll.Name = "btn_ShowAll"
        Me.btn_ShowAll.Size = New System.Drawing.Size(155, 23)
        Me.btn_ShowAll.TabIndex = 1
        Me.btn_ShowAll.Text = "Show &all results"
        Me.btn_ShowAll.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(929, 276)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(80, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Unit: Bytes/sec"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'btn_LocalHostfilter
        '
        Me.btn_LocalHostfilter.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btn_LocalHostfilter.Location = New System.Drawing.Point(301, 271)
        Me.btn_LocalHostfilter.Name = "btn_LocalHostfilter"
        Me.btn_LocalHostfilter.Size = New System.Drawing.Size(155, 23)
        Me.btn_LocalHostfilter.TabIndex = 5
        Me.btn_LocalHostfilter.Text = "Filter &local IP address"
        Me.btn_LocalHostfilter.UseVisualStyleBackColor = True
        '
        'btn_RemoteHostfilter
        '
        Me.btn_RemoteHostfilter.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btn_RemoteHostfilter.Location = New System.Drawing.Point(589, 271)
        Me.btn_RemoteHostfilter.Name = "btn_RemoteHostfilter"
        Me.btn_RemoteHostfilter.Size = New System.Drawing.Size(155, 23)
        Me.btn_RemoteHostfilter.TabIndex = 7
        Me.btn_RemoteHostfilter.Text = "Filter &remote IP address"
        Me.btn_RemoteHostfilter.UseVisualStyleBackColor = True
        '
        'cb_LocalIPs
        '
        Me.cb_LocalIPs.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cb_LocalIPs.FormattingEnabled = True
        Me.cb_LocalIPs.Location = New System.Drawing.Point(174, 273)
        Me.cb_LocalIPs.Name = "cb_LocalIPs"
        Me.cb_LocalIPs.Size = New System.Drawing.Size(121, 21)
        Me.cb_LocalIPs.TabIndex = 8
        '
        'cb_RemoteIPs
        '
        Me.cb_RemoteIPs.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cb_RemoteIPs.FormattingEnabled = True
        Me.cb_RemoteIPs.Location = New System.Drawing.Point(462, 273)
        Me.cb_RemoteIPs.Name = "cb_RemoteIPs"
        Me.cb_RemoteIPs.Size = New System.Drawing.Size(121, 21)
        Me.cb_RemoteIPs.TabIndex = 9
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DateiToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 24)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(1059, 24)
        Me.MenuStrip1.TabIndex = 11
        '
        'DateiToolStripMenuItem
        '
        Me.DateiToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ErgebnisseexportierenToolStripMenuItem, Me.FensterschließenToolStripMenuItem})
        Me.DateiToolStripMenuItem.Name = "DateiToolStripMenuItem"
        Me.DateiToolStripMenuItem.Size = New System.Drawing.Size(12, 20)
        '
        'ErgebnisseexportierenToolStripMenuItem
        '
        Me.ErgebnisseexportierenToolStripMenuItem.Name = "ErgebnisseexportierenToolStripMenuItem"
        Me.ErgebnisseexportierenToolStripMenuItem.Size = New System.Drawing.Size(67, 22)
        '
        'FensterschließenToolStripMenuItem
        '
        Me.FensterschließenToolStripMenuItem.Name = "FensterschließenToolStripMenuItem"
        Me.FensterschließenToolStripMenuItem.Size = New System.Drawing.Size(67, 22)
        '
        'MenuStrip2
        '
        Me.MenuStrip2.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DateiToolStripMenuItem1})
        Me.MenuStrip2.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip2.Name = "MenuStrip2"
        Me.MenuStrip2.Size = New System.Drawing.Size(1059, 24)
        Me.MenuStrip2.TabIndex = 12
        Me.MenuStrip2.Text = "MenuStrip2"
        '
        'DateiToolStripMenuItem1
        '
        Me.DateiToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ExportAlsCSVDateiToolStripMenuItem, Me.FensterschließenToolStripMenuItem1})
        Me.DateiToolStripMenuItem1.Name = "DateiToolStripMenuItem1"
        Me.DateiToolStripMenuItem1.Size = New System.Drawing.Size(37, 20)
        Me.DateiToolStripMenuItem1.Text = "&File"
        '
        'ExportAlsCSVDateiToolStripMenuItem
        '
        Me.ExportAlsCSVDateiToolStripMenuItem.Name = "ExportAlsCSVDateiToolStripMenuItem"
        Me.ExportAlsCSVDateiToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.ExportAlsCSVDateiToolStripMenuItem.Text = "Export as &CSV file"
        '
        'FensterschließenToolStripMenuItem1
        '
        Me.FensterschließenToolStripMenuItem1.Name = "FensterschließenToolStripMenuItem1"
        Me.FensterschließenToolStripMenuItem1.Size = New System.Drawing.Size(180, 22)
        Me.FensterschließenToolStripMenuItem1.Text = "&Close window"
        '
        'ShowResults
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1059, 303)
        Me.Controls.Add(Me.cb_RemoteIPs)
        Me.Controls.Add(Me.cb_LocalIPs)
        Me.Controls.Add(Me.btn_RemoteHostfilter)
        Me.Controls.Add(Me.btn_LocalHostfilter)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btn_ShowAll)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.MenuStrip2)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "ShowResults"
        Me.Text = "Results"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.MenuStrip2.ResumeLayout(False)
        Me.MenuStrip2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents BindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents btn_ShowAll As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btn_LocalHostfilter As System.Windows.Forms.Button
    Friend WithEvents btn_RemoteHostfilter As System.Windows.Forms.Button
    Friend WithEvents cb_LocalIPs As System.Windows.Forms.ComboBox
    Friend WithEvents cb_RemoteIPs As System.Windows.Forms.ComboBox
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents DateiToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ErgebnisseexportierenToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FensterschließenToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents MenuStrip2 As System.Windows.Forms.MenuStrip
    Friend WithEvents DateiToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExportAlsCSVDateiToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FensterschließenToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem

End Class

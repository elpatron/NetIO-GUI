﻿Imports System.Net
Imports System.Net.NetworkInformation
Imports System.Text

Public Class TraceRoute
    Private Const Data As String = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"

    Public Shared Function GetTraceRoute(ByVal hostNameOrAddress As String) As IEnumerable(Of IPAddress)
        Return GetTraceRoute(hostNameOrAddress, 1)
    End Function

    Private Shared Function GetTraceRoute(ByVal hostNameOrAddress As String, ByVal ttl As Integer) As List(Of IPAddress)
        Dim pinger As Ping = New Ping
        Dim pingerOptions As PingOptions = New PingOptions(ttl, True)
        Dim timeout As Integer = 10000
        Dim buffer() As Byte = Encoding.ASCII.GetBytes(Data)
        Dim reply As PingReply
        Dim result As List(Of IPAddress) = New List(Of IPAddress)

        Try
            reply = pinger.Send(hostNameOrAddress, timeout, buffer, pingerOptions)

            If reply.Status = IPStatus.Success Then
                result.Add(reply.Address)
            ElseIf reply.Status = IPStatus.TtlExpired Then
                'add the currently returned address
                result.Add(reply.Address)
                'recurse to get the next address...
                Dim tempResult As IEnumerable(Of IPAddress)
                tempResult = GetTraceRoute(hostNameOrAddress, ttl + 1)
                result.AddRange(tempResult)
            Else
                'failure 
            End If

        Catch ex As Exception
            MsgBox(ex.Message + ":" + vbCrLf + ex.InnerException.Message, MsgBoxStyle.Exclamation)
        End Try

        Return result
    End Function
End Class
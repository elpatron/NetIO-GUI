Download latest version of netio (win32-i686.exe and win32-x86_64.exe) from 
https://github.com/kai-uwe-rommel/netio/releases

- rename win32-i686.exe to netio_win32-i686.exe
- rename win32-x86_64.exe to netio_win32-x86_64.exe
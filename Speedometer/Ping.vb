﻿Imports System
Imports System.Net
Imports System.Net.NetworkInformation
Imports System.Text
Imports NLog
Imports System.Math

Namespace Markus.System.Net.NetworkInformation.Myping
    Public Class _Ping
        ' args[0] can be an IPaddress or host name.
        Public Shared Function StartPing(ByVal str_destination As String, Optional ByVal int_bufferlength As Integer = 32, _
                                         Optional ByVal int_retries As Integer = 1) As Integer
            'Dim logger As Logger = LogManager.GetCurrentClassLogger()
            Dim logger As Logger = LogManager.GetLogger("Ping")
            Dim pingSender As New Ping()
            Dim options As New PingOptions()
            Dim rtt As Integer = 0
            Logger.Info("Starting ping to " & str_destination)
            ' Use the default Ttl value which is 128,
            ' but change the fragmentation behavior.
            options.DontFragment = True

            ' Create a buffer of 32 bytes of data to be transmitted.
            Dim data As String = Nothing
            For count As Integer = 1 To int_bufferlength
                data += "a"
            Next
            Dim buffer() As Byte = Encoding.ASCII.GetBytes(data)
            Dim timeout As Integer = 120
            For count As Integer = 0 To int_retries
                Try
                    Dim reply As PingReply = pingSender.Send(str_destination, timeout, buffer, options)
                    If reply.Status = IPStatus.Success Then
                        logger.Debug("Address: {0}", reply.Address.ToString())
                        logger.Debug("RoundTrip time: {0}", reply.RoundtripTime)
                        logger.Debug("Time to live: {0}", reply.Options.Ttl)
                        logger.Debug("Don't fragment: {0}", reply.Options.DontFragment)
                        logger.Debug("Buffer size: {0}", reply.Buffer.Length)
                        If reply.RoundtripTime >= 0 Then
                            rtt += reply.RoundtripTime
                        End If
                    Else
                        rtt = -1
                    End If
                Catch ex As Exception
                    MsgBox(ex.Message + ":" + vbCrLf + ex.InnerException.Message, MsgBoxStyle.Exclamation)
                    Exit For
                End Try

                'Application.DoEvents()
                'Threading.Thread.Sleep(200)
            Next
            If rtt >= 0 Then
                rtt = CInt(Round(rtt / int_retries))
            End If
            Return rtt
        End Function
    End Class
End Namespace

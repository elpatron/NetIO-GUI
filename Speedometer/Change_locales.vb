﻿Imports System.Globalization
Imports System.Threading
Imports System.ComponentModel

Module Change_locales
    Public Sub ApplyLocale(ByVal locale_name As String, ByVal form As System.Windows.Forms.Form)
        ' Make a CultureInfo and ComponentResourceManager.
        Dim culture_info As New CultureInfo(locale_name)
        Dim component_resource_manager As New ComponentResourceManager(form.GetType)

        ' Make the thread use this locale. This doesn't change
        ' existing controls but will apply to those loaded later
        ' and to messages we get for Help About (see below).
        Thread.CurrentThread.CurrentUICulture = culture_info
        Thread.CurrentThread.CurrentCulture = culture_info

        ' Apply the locale to the form itself.
        ' Debug.WriteLine("$this")
        component_resource_manager.ApplyResources(form, "$this", culture_info)

        ' Apply the locale to the form's controls.
        For Each ctl As Control In form.Controls
            ApplyLocaleToControl(ctl, component_resource_manager, culture_info)
        Next ctl
    End Sub

    Private Sub ApplyLocaleToControl(ByVal ctl As Control, ByVal component_resource_manager As ComponentResourceManager, ByVal culture_info As CultureInfo)
        ' Debug.WriteLine(ctl.Name)
        component_resource_manager.ApplyResources(ctl, ctl.Name, culture_info)

        ' See what kind of control this is.
        If TypeOf ctl Is MenuStrip Then
            ' Apply the new locale to the MenuStrip's items.
            Dim menu_strip As MenuStrip = DirectCast(ctl, MenuStrip)
            For Each child As ToolStripMenuItem In menu_strip.Items
                ApplyLocaleToToolStripItem(child, component_resource_manager, culture_info)
            Next child
        Else
            ' Apply the new locale to the control's children.
            For Each child As Control In ctl.Controls
                ApplyLocaleToControl(child, component_resource_manager, culture_info)
            Next child
        End If
    End Sub

    Private Sub ApplyLocaleToToolStripItem(ByVal item As ToolStripItem, ByVal component_resource_manager As ComponentResourceManager, ByVal culture_info As CultureInfo)
        ' Debug.WriteLine(menu_item.Name)
        component_resource_manager.ApplyResources(item, item.Name, culture_info)

        ' Apply the new locale to items contained in it.
        If TypeOf item Is ToolStripMenuItem Then
            Dim menu_item As ToolStripMenuItem = DirectCast(item, ToolStripMenuItem)
            For Each child As ToolStripItem In menu_item.DropDownItems
                ApplyLocaleToToolStripItem(child, component_resource_manager, culture_info)
            Next child
        End If
    End Sub

End Module

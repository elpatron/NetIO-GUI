﻿Imports System.IO
' Imports Microsoft.Office.Interop
Imports System.Threading
Imports NLog

Module exportdata
    Private logger As Logger = LogManager.GetCurrentClassLogger()
    Private currentCulture As String = Form1.str_currentCulture
    ''' <summary>
    ''' Schreibt den Inhalt einer DataTable in eine CSV Datei
    ''' </summary>
    ''' <param name="str_filename">Pfad der CSV Datei</param>
    ''' <param name="datatable">die zu schreibene DataTable</param>
    ''' <param name="seperator">Zeichen mit dem die Spalten getrennt werden. Meist ';' oder ','</param>
    Public Sub WriteDataTable(str_filename As String, datatable As DataTable, seperator As Char)
        logger.Info("exporting results to CSV file " + str_filename)
        Try
            Using sw As New StreamWriter(str_filename, False, System.Text.Encoding.[Default])
                Dim numberOfColumns As Integer = datatable.Columns.Count

                For i As Integer = 0 To numberOfColumns - 1
                    sw.Write(datatable.Columns(i))
                    If i < numberOfColumns - 1 Then
                        sw.Write(seperator)
                    End If
                Next
                sw.Write(sw.NewLine)

                For Each dr As DataRow In datatable.Rows
                    For i As Integer = 0 To numberOfColumns - 1
                        sw.Write(dr(i).ToString())

                        If i < numberOfColumns - 1 Then
                            sw.Write(seperator)
                        End If
                    Next
                    sw.Write(sw.NewLine)
                Next
            End Using
        Catch ex As Exception
            logger.Error("an error occurred writing to csv file " + str_filename + ": " + ex.Message)
        End Try
    End Sub

End Module

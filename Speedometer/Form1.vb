﻿Imports System.Threading
Imports System.Diagnostics
Imports System.ComponentModel
Imports System.Security.AccessControl
Imports System.Globalization
Imports NLog
Imports System.IO
Imports System.Math
Imports System.Text.RegularExpressions
Imports System.Net
Imports System.Net.Dns
Imports System.Net.Http
Imports System.Threading.Tasks
Imports System.Data.SQLite
Imports System.Management
Imports System.Reflection
Imports System.Net.NetworkInformation
Imports System.Linq
Imports RestSharp

Public Class Form1
    Private str_version As String = Assembly.GetEntryAssembly().GetName().Version.ToString()
    Private rx_results(6) As String
    Private tx_results(6) As String
    Private int_avg_ping As Integer = 0
    Private str_myIP_2_destination As String
    Private int_successful_ping As Integer = 0
    Private int_stepcounter As Integer = -1
    Private int_progress As Integer
    Private str_protocol As String
    Private str_port As String
    Private str_comment As String
    Private int_Runs As Integer = 1
    Private int_rating As Integer = 0
    Private int_proc_netio_id As Integer
    Private bol_servermode As Boolean
    Private bol_setting_checkboxes As Boolean
    Private logger As Logger = LogManager.GetCurrentClassLogger()
    Private str_mydir As String = Path.GetDirectoryName(Application.ExecutablePath)
    Private str_datadir As String
    Private str_inifile As String = str_mydir + "\NetIO-GUI.ini"
    Private str_logfile As String = str_mydir + "\NetIO-GUI.log"
    Private bPortablemode As Boolean = True
    Private sNetioExe As String
    Public str_dbfilename As String = "NetIO-GUI.sqlite"
    Public str_currentCulture As String


    Public Sub New()
        ' Schreibrechte vorhanden?
        Dim UAR As New UserAccessRights(str_mydir)
        Dim AppPath As String = Assembly.GetExecutingAssembly().Location
        Dim AppDirectory = IO.Path.GetDirectoryName(AppPath)
        ' Falls keine Schreibrechte oder Kommandoparameter -a: non-portable Modus, INI, Datenbank und Logfile in %APPDATA%\NetIO-GUI\
        If UAR.CanCreateFiles = False Or cmd_IsSet("-a") = True Or
                str_mydir.ToLower.Contains(Environment.GetEnvironmentVariable("PROGRAMFILES").ToLower) Then

            str_datadir = Environment.GetEnvironmentVariable("APPDATA") + "\NetIO-GUI"
            bPortablemode = False
            If Not Directory.Exists(str_datadir) Then
                Try
                    Directory.CreateDirectory(str_datadir)
                Catch ex2 As Exception

                End Try
                ' Logfile umbiegen
                Dim target As Targets.FileTarget = LogManager.Configuration.FindTargetByName("file")
                target.FileName = str_datadir + "\NetIO-GUI.log"
                str_logfile = str_datadir + "\NetIO-GUI.log"
                ' INI-File und DB file setzen
                str_inifile = str_datadir + "\NetIO-GUI.ini"
                str_dbfilename = str_datadir + "\NetIO-GUI.sqlite"
            End If
        End If

        logger.Info("Starting NetIO-GUI v" + str_version)
        logger.Info("Portable mode: " + bPortablemode.ToString)
        logger.Debug("Using INI file: " + str_inifile)
        logger.Debug("Using DB file: " + str_dbfilename)
        logger.Debug("Using Log file: " + str_logfile)


        If Environment.Is64BitOperatingSystem Then
            sNetioExe = Path.Combine(AppDirectory, "netio", "netio_win32-i686.exe")
        Else
            sNetioExe = Path.Combine(AppDirectory, "netio", "netio_win32-x86_64.exe")
        End If

        ' initialize INI-Reader
        Dim ir As IniReader = New IniReader(str_inifile)

        Me.InitializeComponent()

        ' Main form title
        Me.Text = Me.Text + " v." + str_version
        If bPortablemode = True Then
            Me.Text = Me.Text + " (portable mode)"
        Else
            Me.Text = Me.Text + " (installed mode)"
        End If

        ' Toolstrip2 / lokal IP-adress(es)
        Dim str_tssl2 As String = "IP(s): "
        If Get_Local_IPs(True, True).Count > 1 Then
            For Each str_ip As String In Get_Local_IPs(True, True)
                str_tssl2 += str_ip
            Next
        Else
            str_tssl2 = Get_Local_IPs(True, False)(0)
        End If

        ' all IPs as Tooltip
        ToolStripStatusLabel2.ToolTipText = str_tssl2
        ' shorten Toolstriptext a to max 30 chars
        If str_tssl2.Length >= 30 Then
            str_tssl2 = str_tssl2.Substring(0, 30) + " (...)"
        End If
        ToolStripStatusLabel2.Text = str_tssl2

        ' INI-Datei aus Kommandozeile überschreibt default?
        If cmd_IsSet("-i") Then
            str_inifile = cmd_ReturnValue("-i")
            logger.Debug("Overwriting INI file from cmd: " + str_inifile)
        End If

        ' ggf. Neue INI-Datei anlegen
        If Not File.Exists(str_inifile) Then
            logger.Debug("Creating new INI file: " + str_inifile)
            ir.Write("Settings", "lastip", "")
            ir.Write("Settings", "databasefile", str_dbfilename)
            ir.Write("Settings", "autostartserver", False)
            ir.Write("Settings", "serverport", "18767")
            ir.Write("Settings", "protocol", "tcp")
            ir.Write("Settings", "language", str_currentCulture)
            ir.Write("Settings", "startupmode", "client")
            ir.Write("Settings", "macvendorsapikey", "")
        End If

        ' Letzte IP-Adresse aus INI Datei
        Dim str_lastip As String = ir.ReadString("Settings", "lastip", "")
        If str_lastip <> "" Then
            txt_IP.Text = str_lastip
        End If

        ' IP aus Kommandozeilenparameter (überschreibt INI)?
        If cmd_IsSet("-z") Then
            txt_IP.Text = cmd_ReturnValue("-z")
            logger.Debug("Overwriting last IP from cmd: " + txt_IP.Text)
        End If

        ' Port aus INI-Datei
        txt_port.Text = ir.ReadString("Settings", "serverport", "18767")
        str_port = txt_port.Text

        ' Protokoll aus INI Datei
        str_protocol = ir.ReadString("Settings", "protocol", "tcp")
        If str_protocol = "tcp" Then
            rb_TCP.Checked = True
        Else
            rb_UDP.Checked = True
        End If

        ' Datenbankdatei aus INI
        str_dbfilename = ir.ReadString("Settings", "databasefile", str_datadir + "\NetIO-GUI.sqlite")
        logger.Debug("Changed DB-file from INI: " + str_dbfilename)

        ' Datenbankdatei aus Kommandozeilenparameter (überschreibt INI)?
        If cmd_IsSet("-d") Then
            str_dbfilename = cmd_ReturnValue("-d")
            logger.Debug("Overwriting DB file from cmd: " + str_dbfilename)
        End If

        ' Datenbankdatei vorhanden? - Ggf. anlegen
        If Not File.Exists(str_dbfilename) Then
            ' Valid directory?
            Dim sDBPath As String = Path.GetDirectoryName(str_dbfilename)
            If sDBPath = "" Then
                sDBPath = str_mydir
            End If
            If Not Directory.Exists(sDBPath) Then
                Try
                    Directory.CreateDirectory(str_mydir)
                    logger.Debug("Database directory created: " + str_mydir)
                Catch ex As Exception
                    logger.Error("Database directory doesn´t exist: " + sDBPath)
                    MsgBox(String.Format("Database directory {0} doesn´t exist and can't be created!{1}{1}{2}", sDBPath, Environment.NewLine, ex.Message), MsgBoxStyle.Critical)
                    End
                End Try
            End If
            logger.Info("Creating new database file " + str_dbfilename)
            sqlite_createdatabase(str_dbfilename)
        End If

        ' Erster Start: About-Box zeigen
        If ir.ReadBoolean("Settings", "showhint", True) = True Then
            show_about()
            ir.Write("Settings", "showhint", False)
        End If

        ' netio.exe vorhanden?
        If Not File.Exists(sNetioExe) Then
            logger.Warn(sNetioExe + " not found")
            MsgBox("The file '" + sNetioExe + "' was not found!", MsgBoxStyle.Critical)
        End If

        ' winserv.exe vorhanden?
        If Not File.Exists(Path.Combine(str_mydir, "winserv.exe")) Then
            logger.Warn(str_mydir + "\winserv.exe not found")
            MsgBox("The file 'winserv.exe' was not found.", MsgBoxStyle.Information)
            NetIOServerdienstToolStripMenuItem.Enabled = False
        End If

        ' Autostart Servermodus?
        If ir.ReadBoolean("Settings", "autostartserver", False) = True Then
            logger.Info("Launching server mode from autostart")
            rb_Server.Checked = True
            str_port = txt_port.Text
            btn_Start.Text = "Stop server"
            ToolStripStatusLabel1.Text = "Server started"
            toggle_mode(True)
            If Not bgw_netio.IsBusy Then
                bgw_netio.RunWorkerAsync()
            End If
        End If
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        logger.Debug("loading main form.")
        ErgebnisseSpeichernToolStripMenuItem.Enabled = False
        ToolStripStatusLabel4.Text = "DB: " + str_dbfilename
        StatusStrip1.ShowItemToolTips = True
        ToolStripStatusLabel4.ToolTipText = "Database file: " + str_dbfilename
        logger.Debug("database file: " + str_dbfilename)

        ' CheckedListbox / lokale IP-Adressen
        Dim localIPS = Get_Local_IPs(True, False).ToArray
        For Each ipadress In localIPS
            logger.Debug("added local IP: " + ipadress)
        Next
        clb_IPs.Items.AddRange(localIPS)
        set_all_ips_checked(True)

        ToolStripStatusLabel1.Text = "running idle"

        ' Maßeinheit Standard KBytes/s
        cb_measure.SelectedIndex = 1
        grp_ResultNetIO.Text = "Results NetIO [KBytes/s]"

        ' In welchem Modus starten?
        Dim ir As IniReader = New IniReader(str_inifile)
        If ir.ReadString("Settings", "startupmode", "client").ToLower = "server" Then
            rb_Server.Checked = True
            toggle_mode(True)
            logger.Debug("activated server mode")
        Else
            rb_Client.Checked = True
            toggle_mode(False)
            logger.Debug("activated client mode")
        End If
        logger.Debug("everything´s fine.")
    End Sub

    Private Sub bgw_netio_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgw_netio.DoWork
        Dim bw As BackgroundWorker = CType(sender, BackgroundWorker)
        e.Result = start_netio(bw, txt_IP.Text)
        If bw.CancellationPending Then
            e.Cancel = True
        End If
    End Sub

    Private Sub bgw_netio_RunWorkerCompleted(ByVal sender As System.Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgw_netio.RunWorkerCompleted
        logger.Info("netio.exe exit code: " + e.Result.ToString())

        ' Bei Erfolg und einfachem Durchlauf: Result in Datenbank        
        If nmRuns.Value = 1 Then
            If e.Result = 0 And tx_results(6) > 0 Then
                Dim answer As DialogResult = CommentResult.ShowDialog
                If answer = Windows.Forms.DialogResult.OK Then
                    str_comment = CommentResult.txt_Comment.Text
                End If
                If CommentResult.rb_rate1.Checked = True Then int_rating = 1
                If CommentResult.rb_rate2.Checked = True Then int_rating = 2
                If CommentResult.rb_rate3.Checked = True Then int_rating = 3
                If CommentResult.rb_rate4.Checked = True Then int_rating = 4
                If CommentResult.rb_rate5.Checked = True Then int_rating = 5

                If rb_Client.Checked = True Then
                    ' save results
                    sqlite_addresults2db(str_dbfilename)
                End If
            End If
        End If

        If int_Runs = nmRuns.Value Then

        End If
        txt_port.Enabled = True
        rb_Client.Enabled = True
        rb_Server.Enabled = True
        ProgressBar1.Value = 0
        btn_Start.Enabled = True

        If e.Result = -2 Then
            logger.Info("netio server doesn´t answer.")
            ToolStripStatusLabel1.Text = "No answer from netio server."
            MsgBox("No answer from netio server.", MsgBoxStyle.Critical)
        End If

        If e.Result = -3 Then
            logger.Info("netio server task is running yet")
            ToolStripStatusLabel1.Text = "The netio server task is running yet."
            Dim answer As MsgBoxResult = MsgBox("The netio server task is running yet. " +
                "Eventually netio was installed as service?. Shall I try to kill " +
                "the process (not the service)?", MsgBoxStyle.YesNo)
            If answer = MsgBoxResult.Yes Then
                Try
                    Dim proc_netio As Process = Process.GetProcessById(int_proc_netio_id)
                    proc_netio.Kill()
                Catch ex As Exception
                    logger.Warn("error killing process (task still running): " + ex.Message)
                End Try
            End If
            ToolStripStatusLabel1.Text = "running idle"
        End If

        If rb_Client.Checked = True Then
            btn_Start.Text = "Start measure"
            ToolStripStatusLabel1.Text = "Measurement finished."
            txt_IP.Enabled = True
            cb_measure.Enabled = True
            rb_TCP.Enabled = True
            rb_UDP.Enabled = True
            cb_NetIO.Enabled = True
            cb_Ping.Enabled = True
            int_stepcounter = -1
        Else
            btn_Start.Text = "Start server"
            ToolStripStatusLabel1.Text = "Server stopped"
        End If

        If bol_servermode = False Then
            ErgebnisseSpeichernToolStripMenuItem.Enabled = True
        End If
    End Sub

    Private Function start_netio(ByVal bw As BackgroundWorker, ByVal destination As String) As Integer
        Dim result As Integer = -1

        Dim pr_netio As Process = New Process
        Dim args As String = "-p " + str_port + " "

        Dim rx_split_index As Integer
        If str_protocol = "tcp" Then
            rx_split_index = 7
        Else
            rx_split_index = 8
        End If

        pr_netio.StartInfo.FileName = sNetioExe
        pr_netio.StartInfo.CreateNoWindow = True
        pr_netio.StartInfo.UseShellExecute = False
        pr_netio.StartInfo.RedirectStandardOutput = True
        pr_netio.StartInfo.RedirectStandardError = True

        If rb_Server.Checked = True Then
            bol_servermode = True
            ' Servermodus
            args += "-s"
            ' ggf. an eine bestimmte IP gebunden
            With clb_IPs
                If Not .Items.Count = .CheckedItems.Count Then
                    For i As Integer = 0 To .Items.Count - 1
                        If .GetItemCheckState(i) = CheckState.Checked Then
                            args += " -h " + .Items.Item(i).ToString
                            Exit For
                        End If
                    Next
                End If
            End With
        Else
            ' netio-Messung in Bytes/sec
            bol_servermode = False
            args += "-B "
            If rb_TCP.Checked = True Then
                args += "-t"
            Else
                args += "-u"
            End If
            args += " " + txt_IP.Text
        End If

        pr_netio.StartInfo.Arguments = args

        If bol_servermode = False Then
            bw.ReportProgress(Round(200 / 8))
        End If

        logger.Info("starting netio.exe w/ arguments : " + args)
        pr_netio.Start()
        int_proc_netio_id = pr_netio.Id
        Dim output As String
        Dim myStreamReader As StreamReader = pr_netio.StandardOutput

        ' Ausgabe im Client Modus interpretieren
        If bol_servermode = False Then
            While Not myStreamReader.EndOfStream
                output = myStreamReader.ReadLine

                ' NetIO-Server antwortet nicht
                If output.Contains("code 100") Then
                    Return -2
                    Exit Function
                End If

                ' Doppelte Leerzeichen gegen einfache ersetzen
                If output.Contains("  ") Then
                    output = output.Replace("  ", " ")
                End If

                logger.Debug("netio client output: " + output)

                ' Parse netio´s stdout
                Try
                    If output.Contains("Packet size 1k bytes:") Then
                        int_stepcounter = 0
                        tx_results(0) = output.Split(" ")(4)
                        rx_results(0) = output.Split(" ")(rx_split_index)
                        logger.Debug(String.Format("Result 1k: {0} rx, {1} tx", tx_results(0), rx_results(0)))
                        bw.ReportProgress(Round(300 / 8))
                    End If

                    If output.Contains("Packet size 2k bytes:") Then
                        int_stepcounter = 1
                        tx_results(1) = output.Split(" ")(4)
                        rx_results(1) = output.Split(" ")(rx_split_index)
                        logger.Debug(String.Format("Result 2k: {0} rx, {1} tx", tx_results(1), rx_results(1)))
                        bw.ReportProgress(Round(400 / 8))
                    End If

                    If output.Contains("Packet size 4k bytes:") Then
                        int_stepcounter = 2
                        tx_results(2) = output.Split(" ")(4)
                        rx_results(2) = output.Split(" ")(rx_split_index)
                        logger.Debug(String.Format("Result 4k: {0} rx, {1} tx", tx_results(2), rx_results(2)))
                        bw.ReportProgress(Round(500 / 8))
                    End If

                    If output.Contains("Packet size 8k bytes:") Then
                        int_stepcounter = 3
                        tx_results(3) = output.Split(" ")(4)
                        rx_results(3) = output.Split(" ")(rx_split_index)
                        logger.Debug(String.Format("Result 8k: {0} rx, {1} tx", tx_results(3), rx_results(3)))
                        bw.ReportProgress(Round(600 / 8))
                    End If

                    If output.Contains("Packet size 16k bytes:") Then
                        int_stepcounter = 4
                        tx_results(4) = output.Split(" ")(4)
                        rx_results(4) = output.Split(" ")(rx_split_index)
                        logger.Debug(String.Format("Result 16k: {0} rx, {1} tx", tx_results(4), rx_results(4)))
                        bw.ReportProgress(Round(700 / 8))
                    End If

                    If output.Contains("Packet size 32k bytes:") Then
                        int_stepcounter = 5
                        tx_results(5) = output.Split(" ")(4)
                        rx_results(5) = output.Split(" ")(rx_split_index)
                        logger.Debug(String.Format("Result 32k: {0} rx, {1} tx", tx_results(5), rx_results(5)))
                        bw.ReportProgress(Round(800 / 8))
                    End If
                Catch ex As Exception
                    logger.Warn("error parsing netio´s stdout: " + ex.Message)
                End Try
            End While

            If nmRuns.Value <> 1 Then
                int_Runs += 1
                sqlite_addresults2db(str_dbfilename)
            End If
        Else
            ' Ausgabe im Server Modus interpretieren
            ' funktioniert irgendwie nicht, weder StandardOutput noch StandardError enthalten Daten?!
            While Not pr_netio.StandardOutput.EndOfStream
                output = myStreamReader.ReadToEnd
                ' netio Server läuft schon
                If output.Contains("bind():") Then
                    Return -3
                    Exit Function
                End If
                logger.Debug("netio server output: " + output)
            End While
        End If

        pr_netio.WaitForExit()
        result = pr_netio.ExitCode
        pr_netio.Close()

        Return result
    End Function

    Private Sub bgw_netio_ProgressChanged(ByVal sender As System.Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs) Handles bgw_netio.ProgressChanged
        ProgressBar1.Value = e.ProgressPercentage
        Dim int_factor As Integer = 1

        ' display value * factor
        Select Case cb_measure.Text.Substring(0, 1)
            Case "K"
                int_factor = 1024
            Case "M"
                int_factor = 1024 ^ 2
            Case "G"
                int_factor = 1024 ^ 3
        End Select

        If int_stepcounter = 0 Then
            lbl_netio_1k_TX.Text = Round((CLng(tx_results(0)) / int_factor), 2).ToString
            lbl_netio_1k_RX.Text = Round((CLng(rx_results(0)) / int_factor), 2).ToString
        End If

        If int_stepcounter = 1 Then
            lbl_netio_2k_TX.Text = Round((CLng(tx_results(1)) / int_factor), 2).ToString
            lbl_netio_2k_RX.Text = Round((CLng(rx_results(1)) / int_factor), 2).ToString
        End If
        If int_stepcounter = 2 Then
            lbl_netio_4k_TX.Text = Round((CLng(tx_results(2)) / int_factor), 2).ToString
            lbl_netio_4k_RX.Text = Round((CLng(rx_results(2)) / int_factor), 2).ToString
        End If
        If int_stepcounter = 3 Then
            lbl_netio_8k_TX.Text = Round((CLng(tx_results(3)) / int_factor), 2).ToString
            lbl_netio_8k_RX.Text = Round((CLng(rx_results(3)) / int_factor), 2).ToString
        End If
        If int_stepcounter = 4 Then
            lbl_netio_16k_TX.Text = Round((CLng(tx_results(4)) / int_factor), 2).ToString
            lbl_netio_16k_RX.Text = Round((CLng(rx_results(4)) / int_factor), 2).ToString
        End If
        If int_stepcounter = 5 Then
            lbl_netio_32k_TX.Text = Round((CLng(tx_results(5)) / int_factor), 2).ToString
            lbl_netio_32k_RX.Text = Round((CLng(rx_results(5)) / int_factor), 2).ToString

            'calc average
            Dim tx_avg As Decimal = 0
            Dim rx_avg As Decimal = 0

            For count As Integer = 0 To 5
                tx_avg += tx_results(count)
                rx_avg += rx_results(count)
            Next

            tx_avg = Round(tx_avg / 6, 0)
            rx_avg = Round(rx_avg / 6, 0)

            tx_results(6) = tx_avg.ToString
            rx_results(6) = rx_avg.ToString

            lbl_avg_tx.Text = Round(tx_avg / int_factor, 2).ToString
            lbl_avg_rx.Text = Round(rx_avg / int_factor, 2).ToString
        End If
    End Sub

    Private Function _ping(ByVal str_ipadress As String, Optional ByVal int_buffersize As Integer = 32, Optional ByVal int_retries As Integer = 1, Optional ByVal countme As Boolean = True) As String
        Dim result As String
        result = Markus.System.Net.NetworkInformation.Myping._Ping.StartPing(str_ipadress, int_buffersize, int_retries)
        If result >= 0 And countme = True Then
            int_successful_ping += 1
            int_avg_ping += result
        End If

        result = result.ToString
        If result = "0" Then
            result = "<1"
        End If
        Return result
    End Function

    Private Sub rb_Server_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_Server.CheckedChanged
        toggle_mode(rb_Server.Checked)
    End Sub

    Private Sub btn_Start_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Start.Click
        bol_servermode = rb_Server.Checked
        If (btn_Start.Text = "Cancel" Or btn_Start.Text = "Stop server") Then
            Dim answer As MsgBoxResult = MsgBox("If you cancel the process, the server process on the " +
                                                "other peer should be restarted.", MsgBoxStyle.YesNo)
            If answer = MsgBoxResult.Yes Then
                logger.Debug("Killing netio task with ID: " + int_proc_netio_id.ToString)
                Try
                    Dim proc_netio As Process = Process.GetProcessById(int_proc_netio_id)
                    If bgw_netio.IsBusy Then
                        Try
                            proc_netio.Kill()
                        Catch ex As Exception
                            logger.Warn("Failed killing " + proc_netio.ProcessName)
                        End Try
                        logger.Debug("Successfully killed netio task")
                    End If
                Catch ex As Exception
                    logger.Warn("Failed to get process by ID")
                End Try

                If rb_Client.Checked = True Then
                    btn_Start.Text = "Start measurement"
                    ToolStripStatusLabel1.Text = "Measurement canceled"
                Else
                    btn_Start.Text = "Start server"
                    ToolStripStatusLabel1.Text = "Server stopped"
                End If
                Exit Sub
            End If
        End If

        If bol_servermode = False Then
            ' Keiner der Tests aktiviert?
            If cb_NetIO.Checked = False And cb_Ping.Checked = False Then
                MsgBox("No test selected (Ping / Netio).", MsgBoxStyle.Information)
                Exit Sub
            End If

            ' IP Adresse valid?
            If Not IsIpValid(txt_IP.Text) And rb_Client.Checked = True Then
                MsgBox("No valid IP-Adress.", MsgBoxStyle.Information)
                Exit Sub
            Else
                ' IP-Adresse in INI speichern
                Dim iw As IniReader = New IniReader(str_inifile)
                iw.Write("Settings", "lastip", txt_IP.Text)
            End If
            clear_values(True)

            ' Gegenstelle erreichbar?
            ToolStripStatusLabel1.Text = "Checking availability..."
            If _ping(txt_IP.Text, 32, 8, False) = "-1" Then
                Dim answer As MsgBoxResult = MsgBox(String.Format("No Ping answer from peer {0}. Proceed with netio-test?", txt_IP.Text), MsgBoxStyle.YesNo)
                If answer = MsgBoxResult.No Then
                    ToolStripStatusLabel1.Text = "Peer not available"
                    clear_values(False)
                    If rb_Client.Checked = True Then
                        btn_Start.Text = "Start measure"
                        ToolStripStatusLabel1.Text = "Measurement canceled"
                    Else
                        btn_Start.Text = "Start server"
                        ToolStripStatusLabel1.Text = "Server stopped"
                    End If
                    Exit Sub
                End If
            End If

            ' Ermitteln der lokalen IP-Adresse, die zum Zielhost führt, via tracert
            Dim match As Boolean = False
            For Each str_ipaddress As String In Get_Local_IPs(True, False)
                match = get_ip_to_destination(str_ipaddress, get_Gateway(txt_IP.Text))
                If match = True Then
                    str_myIP_2_destination = str_ipaddress
                    logger.Info("local IP address to destination: " + str_myIP_2_destination)
                    Exit For
                End If
            Next
            If match = False Then
                logger.Warn("couldn´t resolve local IP to destination")
                str_myIP_2_destination = Nothing
            End If

            If cb_Ping.Checked Then
                ' RTT
                ProgressBar1.Value = (Round(100 / 8))
                ToolStripStatusLabel1.Text = "Testing ping performance..."
                lbl_ping_32b.Text = _ping(txt_IP.Text, 32, 3)
                Application.DoEvents()
                lbl_ping_64b.Text = _ping(txt_IP.Text, 64, 3)
                Application.DoEvents()
                lbl_ping_128b.Text = _ping(txt_IP.Text, 128, 3)
                Application.DoEvents()
                lbl_ping_256b.Text = _ping(txt_IP.Text, 256, 3)
                Application.DoEvents()
                lbl_ping_512b.Text = _ping(txt_IP.Text, 512, 3)
                Application.DoEvents()
                lbl_ping_1024b.Text = _ping(txt_IP.Text, 1024, 3)
                If int_successful_ping > 0 Then
                    int_avg_ping = CInt(Round((int_avg_ping / int_successful_ping), 0))
                    If int_avg_ping = 0 Then
                        lbl_avg_ping.Text = "<1"
                    Else
                        lbl_avg_ping.Text = int_avg_ping.ToString
                    End If
                End If
            End If
            ToolStripStatusLabel1.Text = "Testing netio performance..."
        Else
            btn_Start.Text = "Stop server"
            ToolStripStatusLabel1.Text = "Server started"
        End If

        'NetIO
        For iCount As Integer = 1 To CInt(nmRuns.Value)
            If Not bgw_netio.IsBusy Then
                bgw_netio.RunWorkerAsync()
            End If
        Next

    End Sub

    Private Function IsIpValid(ByVal ipAddress As String) As Boolean
        Dim expr As String = "^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$"
        Dim reg As Regex = New Regex(expr)
        If (reg.IsMatch(ipAddress)) Then
            Dim parts() As String = ipAddress.Split(".")
            If Convert.ToInt32(parts(0)) = 0 Then
                Return False
            ElseIf Convert.ToInt32(parts(3)) = 0 Then
                Return False
            End If
            For i As Integer = 0 To 3
                If parts(i) > 254 Then
                    Return False
                End If
            Next
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub Form1_FormClosing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        Try
            Dim proc_netio As Process = Process.GetProcessById(int_proc_netio_id)
            If bgw_netio.IsBusy Then
                proc_netio.Kill()
            End If
        Catch ex As Exception
            logger.Warn("Error killing process (MyBase.FormClosing)")
        End Try
    End Sub

    Private Sub txt_port_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_port.TextChanged
        If CInt(txt_port.Text) < 1 Or CInt(txt_port.Text) > 65536 Then
            txt_port.Text = "18767"
            str_port = "18767"
        Else
            str_port = txt_port.Text
        End If
    End Sub

    Public Function Get_Local_IPs(ByVal onlyv4 As Boolean, ByVal delimit As Boolean, Optional ByVal delimiter As String = ", ") As ArrayList
        Dim str_Al As New ArrayList()
        Dim int_count As Integer = 0
        Try
            Dim IP As Net.IPAddress
            Dim IPList As Array = Net.Dns.GetHostEntry(SystemInformation.ComputerName.ToString).AddressList
            If IPList.Length > 0 Then

                For i As Integer = 0 To IPList.Length - 1
                    IP = IPList.GetValue(i)
                    If onlyv4 = True Then
                        If IP.ToString.Contains(".") Then
                            str_Al.Add(IP.ToString)
                            int_count += 1
                        End If
                    Else
                        'str_Al.Add(IP.ToString)
                        'int_count += 1
                    End If

                    If delimit = True Then
                        If int_count > 0 Then
                            str_Al.Add(delimiter)
                        End If
                    End If
                Next

                ' Remove last delimiter
                ' This seems not to work...
                If delimit = True Then
                    'Try
                    '    str_Al.RemoveAt(int_count * 2 - 1)
                    'Catch ex As Exception

                    'End Try    
                End If
                Return str_Al
            End If
        Catch ex As Exception
        End Try

        Return str_Al
    End Function

    Private Sub clb_IPs_ItemCheck(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs) Handles clb_IPs.ItemCheck
        If bol_setting_checkboxes = True Then
            Exit Sub
        End If
        bol_setting_checkboxes = True
        set_all_ips_checked(False)
        bol_setting_checkboxes = True

        ' Entweder alle oder genau eine Adresse darf gecheckt sein
        If e.NewValue = CheckState.Checked Then
            bol_setting_checkboxes = True
            With clb_IPs
                .BeginUpdate()
                For i As Integer = 0 To .Items.Count - 1
                    If i <> .SelectedIndex Then
                        .SetItemChecked(i, False)
                    End If
                Next
                .EndUpdate()
            End With
        End If
        bol_setting_checkboxes = False
    End Sub

    Private Sub set_all_ips_checked(ByVal checked As Boolean)
        bol_setting_checkboxes = True
        With clb_IPs
            .BeginUpdate()
            For i As Integer = 0 To .Items.Count - 1
                .SetItemChecked(i, checked)
            Next
            .EndUpdate()
        End With
        bol_setting_checkboxes = False
    End Sub

    Private Sub clb_IPs_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles clb_IPs.SelectedIndexChanged
        ' Mindestens eine Adresse muss gecheckt bleiben
        If clb_IPs.CheckedItems.Count = 0 Then
            bol_setting_checkboxes = True
            With clb_IPs
                .BeginUpdate()
                .SetItemChecked(clb_IPs.SelectedIndex, True)
                .EndUpdate()
            End With
        End If
        bol_setting_checkboxes = False
    End Sub

    Private Sub ErgebnisseSpeichernToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ErgebnisseSpeichernToolStripMenuItem.Click
        Dim str_fn As String = Path.Combine(txt_IP.Text, "_results.txt")
        Dim bol_appendfile As Boolean = True

        ' Datei existiert: anhängen, überschreiben oder Abbruch?
        If File.Exists(str_fn) Then
            Dim answer As MsgBoxResult = MsgBox(String.Format("Result file {0} exists. Append results (Yes), overwrite file (No) or cancel?", str_fn), MsgBoxStyle.YesNoCancel)
            Select Case answer
                Case MsgBoxResult.Yes
                    bol_appendfile = True
                Case MsgBoxResult.No
                    bol_appendfile = False
                Case MsgBoxResult.Cancel
                    Exit Sub
            End Select
        End If

        logger.Info("Saving results as textfile: " + str_fn)

        With SaveFileDialog1
            .FileName = str_fn
            .InitialDirectory = str_mydir
            .Filter = "txt files (*.txt)|*.txt"
            .RestoreDirectory = True
            .OverwritePrompt = False
            If .ShowDialog = Windows.Forms.DialogResult.OK Then
                Dim sw As StreamWriter
                Try
                    sw = New StreamWriter(.FileName, bol_appendfile)
                    sw.WriteLine("============================================================================")
                    sw.WriteLine("NetIO results")
                    sw.WriteLine("Date/Time: " + DateTime.Now.ToString)
                    sw.WriteLine("Server IP: " + txt_IP.Text)
                    sw.WriteLine("Server Port: " + txt_port.Text)
                    sw.WriteLine("Local IP to destination: " + str_myIP_2_destination)
                    sw.Write("Protocol: ")
                    If rb_TCP.Checked = True Then
                        sw.WriteLine("TCP")
                    Else
                        sw.WriteLine("UDP")
                    End If
                    sw.WriteLine()
                    sw.WriteLine("----------------------------------------------------------------------------")
                    sw.WriteLine("Ping results")
                    sw.WriteLine("Measurement unit: ms")
                    sw.WriteLine()
                    sw.WriteLine("Bytes;RTT")
                    sw.WriteLine("32;" + lbl_ping_32b.Text)
                    sw.WriteLine("64;" + lbl_ping_64b.Text)
                    sw.WriteLine("128;" + lbl_ping_128b.Text)
                    sw.WriteLine("256;" + lbl_ping_256b.Text)
                    sw.WriteLine("512;" + lbl_ping_512b.Text)
                    sw.WriteLine("1024;" + lbl_ping_1024b.Text)
                    sw.WriteLine("Avg;" + lbl_avg_ping.Text)
                    sw.WriteLine("----------------------------------------------------------------------------")
                    sw.WriteLine("NetIO results")
                    sw.WriteLine("Measurement unit: " + cb_measure.Text)
                    sw.WriteLine()
                    sw.WriteLine("Packetsize;RX;TX")
                    sw.WriteLine(String.Format("1k;{0};{1}", lbl_netio_1k_RX.Text, lbl_netio_1k_TX.Text))
                    sw.WriteLine(String.Format("2k;{0};{1}", lbl_netio_2k_RX.Text, lbl_netio_2k_TX.Text))
                    sw.WriteLine(String.Format("4k;{0};{1}", lbl_netio_4k_RX.Text, lbl_netio_4k_TX.Text))
                    sw.WriteLine(String.Format("8k;{0};{1}", lbl_netio_8k_RX.Text, lbl_netio_8k_TX.Text))
                    sw.WriteLine(String.Format("16k;{0};{1}", lbl_netio_16k_RX.Text, lbl_netio_16k_TX.Text))
                    sw.WriteLine(String.Format("32k;{0};{1}", lbl_netio_32k_RX.Text, lbl_netio_32k_TX.Text))
                    sw.WriteLine(String.Format("Avg TX;{0}", lbl_avg_tx.Text))
                    sw.WriteLine(String.Format("Avg RX;{0}", lbl_avg_rx.Text))
                    sw.Close()
                Catch ex As Exception
                    logger.Warn("Error writing to results file " + .FileName)
                    MsgBox("Error saving results to file.", MsgBoxStyle.Critical)
                End Try
                MsgBox(String.Format("Saved results to {0}.", .FileName), MsgBoxStyle.Information)
            End If
        End With
    End Sub

    Private Sub BeendenToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BeendenToolStripMenuItem.Click
        Me.Close()
    End Sub

    Private Sub show_about()
        MsgBox("NetIO-GUI v" + str_version + Environment.NewLine + Environment.NewLine + "This software licensed under the GNU GPL v3 (see Help - License). " _
                       + "Nevertheless, the included software 'NETIO' is free for personal and educational use only. For details see 'netio.doc'." _
                       + Environment.NewLine + Environment.NewLine + "NetIO-GUI " + Chr(169) + " 2012-2022 M. Busche, m.busche@gmail.com" + Environment.NewLine +
                       "NETIO " + Chr(169) + " 1997-2012 Kai Uwe Rommel, https://github.com/kai-uwe-rommel/netio" + Environment.NewLine + Environment.NewLine _
                       , MsgBoxStyle.OkOnly, "Info")
    End Sub

    Private Sub ÜberNetIOGUIToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ÜberNetIOGUIToolStripMenuItem.Click
        show_about()
    End Sub

    Private Sub LetzteErgebnissevergleichenToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LetzteErgebnissevergleichenToolStripMenuItem.Click
        If File.Exists(str_dbfilename) Then
            ShowResults.ShowDialog()
        Else
            MsgBox("Database file not found.", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub sqlite_addresults2db(ByVal str_dbname As String)
        logger.Info("Saving results to database.")
        Dim int_factor As Integer

        With cb_measure
            If .Text.StartsWith("B") Then
                int_factor = 1 * 1
            End If
            If .Text.StartsWith("K") Then
                int_factor = 1024
            End If
            If .Text.StartsWith("M") Then
                int_factor = 1024 * 1024
            End If
            If .Text.StartsWith("G") Then
                int_factor = 1024 * 1024 * 1024
            End If
        End With

        Dim dt As String = Format(Now, "yyyy-MM-dd HH:mm")
        Dim SQLconnect As New SQLite.SQLiteConnection()
        Dim SQLcommand As SQLiteCommand

        SQLconnect.ConnectionString = "Data Source=" + str_dbname + ";"

        'Insert or replace host record into hosts table
        SQLconnect.Open()
        SQLcommand = SQLconnect.CreateCommand
        SQLcommand.CommandText = String.Format("INSERT INTO 'results' ('ID', 'Local_Host', 'Remote_Host', 'Timestamp', 'Protocol', 'TX_1k', 'TX_2k', 'TX_4k', 'TX_8k', 'TX_16k', 'TX_32k', 'RX_1k', 'RX_2k', " +
                                               "'RX_4k', 'RX_8k', 'RX_16k', 'RX_32k', 'TX_AVG', 'RX_AVG', 'Ping_AVG', 'Comment', 'Rating')" +
                                               " VALUES (NULL, '{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '{11}', '{12}', '{13}', '{14}', '{15}', '{16}', '{17}', '{18}', '{19}', '{20}');",
                                               str_myIP_2_destination, txt_IP.Text, dt, str_protocol.ToUpper, tx_results(0), tx_results(1), tx_results(2), tx_results(3), tx_results(4), tx_results(5),
                                               rx_results(0), rx_results(1), rx_results(2), rx_results(3), rx_results(4), rx_results(5), Round(CDec(tx_results(6)), 2).ToString,
                                               Round(CDec(rx_results(6)), 2).ToString, int_avg_ping.ToString, str_comment, int_rating)
        logger.Debug("SQLcommand: " + SQLcommand.CommandText)

        Try
            SQLcommand.ExecuteNonQuery()
        Catch ex As Exception
            logger.Fatal("Error executing SQLcommand")
        End Try

        SQLcommand = SQLconnect.CreateCommand
        SQLcommand.CommandText = String.Format("INSERT OR REPLACE INTO 'local_ips' ('Local_IP') VALUES ('{0}');", str_myIP_2_destination)
        logger.Debug("SQLcommand: " + SQLcommand.CommandText)

        Try
            SQLcommand.ExecuteNonQuery()
        Catch ex As Exception
            logger.Fatal("Error executing SQLcommand")
        End Try

        SQLcommand = SQLconnect.CreateCommand
        SQLcommand.CommandText = String.Format("INSERT OR REPLACE INTO 'remote_ips' ('Remote_IP') VALUES ('{0}');", txt_IP.Text)
        logger.Debug("SQLcommand: " + SQLcommand.CommandText)

        Try
            SQLcommand.ExecuteNonQuery()
        Catch ex As Exception
            logger.Fatal("Error executing SQLcommand")
        End Try

        SQLcommand.Dispose()
        SQLconnect.Close()
        ToolStripStatusLabel1.Text = "Measurement finished."
    End Sub

    Private Sub cb_measure_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cb_measure.SelectedIndexChanged
        If lbl_netio_1k_TX.Text = "-" Then
            Exit Sub
        End If
        Dim int_factor As Integer = 1
        Dim str_results As String = "Results"
        Select Case cb_measure.Text.Substring(0, 1)
            Case "B"
                grp_ResultNetIO.Text = str_results + " NetIO [Bytes/s]"
            Case "K"
                int_factor = 1024
                grp_ResultNetIO.Text = str_results + " NetIO [KBytes/s]"
            Case "M"
                int_factor = 1024 * 1024
                grp_ResultNetIO.Text = str_results + " NetIO [MBytes/s]"
            Case "G"
                int_factor = 1024 * 1024 * 1024
                grp_ResultNetIO.Text = str_results + " NetIO [GBytes/s]"
        End Select

        lbl_netio_1k_TX.Text = Round((CLng(tx_results(0)) / int_factor), 2).ToString
        lbl_netio_1k_RX.Text = Round((CLng(rx_results(0)) / int_factor), 2).ToString
        lbl_netio_2k_TX.Text = Round((CLng(tx_results(1)) / int_factor), 2).ToString
        lbl_netio_2k_RX.Text = Round((CLng(rx_results(1)) / int_factor), 2).ToString
        lbl_netio_4k_TX.Text = Round((CLng(tx_results(2)) / int_factor), 2).ToString
        lbl_netio_4k_RX.Text = Round((CLng(rx_results(2)) / int_factor), 2).ToString
        lbl_netio_8k_TX.Text = Round((CLng(tx_results(3)) / int_factor), 2).ToString
        lbl_netio_8k_RX.Text = Round((CLng(rx_results(3)) / int_factor), 2).ToString
        lbl_netio_16k_TX.Text = Round((CLng(tx_results(4)) / int_factor), 2).ToString
        lbl_netio_16k_RX.Text = Round((CLng(rx_results(4)) / int_factor), 2).ToString
        lbl_netio_32k_TX.Text = Round((CLng(tx_results(5)) / int_factor), 2).ToString
        lbl_netio_32k_RX.Text = Round((CLng(rx_results(5)) / int_factor), 2).ToString
        lbl_avg_tx.Text = Round((CDec(tx_results(6)) / int_factor), 2).ToString
        lbl_avg_rx.Text = Round((CDec(rx_results(6)) / int_factor), 2).ToString
    End Sub

    Private Sub clear_values(ByVal bol_disable_controls As Boolean)
        ' Reset values and labels
        Array.Clear(tx_results, 0, 6)
        Array.Clear(rx_results, 0, 6)
        int_avg_ping = 0
        int_successful_ping = 0
        lbl_netio_1k_TX.Text = "-"
        lbl_netio_1k_RX.Text = "-"
        lbl_netio_2k_TX.Text = "-"
        lbl_netio_2k_RX.Text = "-"
        lbl_netio_4k_TX.Text = "-"
        lbl_netio_4k_RX.Text = "-"
        lbl_netio_8k_TX.Text = "-"
        lbl_netio_8k_RX.Text = "-"
        lbl_netio_16k_TX.Text = "-"
        lbl_netio_16k_RX.Text = "-"
        lbl_netio_32k_TX.Text = "-"
        lbl_netio_32k_RX.Text = "-"
        lbl_avg_tx.Text = "-"
        lbl_avg_rx.Text = "-"
        lbl_ping_32b.Text = "-"
        lbl_ping_64b.Text = "-"
        lbl_ping_128b.Text = "-"
        lbl_ping_256b.Text = "-"
        lbl_ping_512b.Text = "-"
        lbl_ping_1024b.Text = "-"
        lbl_avg_ping.Text = "-"
        int_progress = 0
        str_comment = ""
        ProgressBar1.Value = 0
        str_port = txt_port.Text
        If rb_Client.Checked = True Then
            btn_Start.Text = "Cancel"
        Else
            btn_Start.Text = "Stop server"
        End If
        ErgebnisseSpeichernToolStripMenuItem.Enabled = False
        If bol_disable_controls = True Then
            cb_measure.Enabled = False
            txt_port.Enabled = False
            rb_Client.Enabled = False
            rb_Server.Enabled = False
            rb_TCP.Enabled = False
            rb_UDP.Enabled = False
            cb_NetIO.Enabled = False
            cb_Ping.Enabled = False
            txt_IP.Enabled = False
        Else
            cb_measure.Enabled = True
            txt_port.Enabled = True
            rb_Client.Enabled = True
            rb_Server.Enabled = True
            rb_TCP.Enabled = True
            rb_UDP.Enabled = True
            cb_NetIO.Enabled = True
            cb_Ping.Enabled = True
            txt_IP.Enabled = True

        End If

        If rb_TCP.Checked = True Then
            str_protocol = "tcp"
        Else
            str_protocol = "udp"
        End If

        If rb_Client.Checked = True Then
            bol_servermode = False
        Else
            bol_servermode = True
        End If
    End Sub

    Private Function get_Gateway(ByVal str_destination As String) As String
        Dim hops As System.Collections.Generic.List(Of IPAddress)
        Dim result As String
        hops = TraceRoute.GetTraceRoute(str_destination)
        Try
            result = hops(0).ToString
        Catch ex As Exception
            logger.Warn("Error resolving gateway.")
            result = Nothing
        End Try
        Return result
    End Function

    Private Function get_ip_to_destination(ByVal str_my_ip As String, ByVal str_gateway As String) As Boolean
        Dim my_ip(3) As String
        Dim gw(3) As String
        Dim result As Boolean = True
        my_ip = str_my_ip.Split(".")
        Try
            gw = str_gateway.Split(".")
        Catch ex As Exception
            result = False
        End Try

        For count As Integer = 0 To 2
            If my_ip(count) <> gw(count) Then
                result = False
            End If
        Next
        Return result
    End Function

    ''' <summary> 
    ''' Installiert, konfiguriert oder deinstalliert netio.exe als Serverdienst
    ''' </summary>
    ''' <param name="p1">action</param>
    ''' <param name="str_prt">TCP/IP-Port des netio Servers</param>
    ''' <returns>False if no error</returns>
    ''' <remarks>action: i-installieren, c-konfigurieren, u-deinstallieren</remarks>
    Private Function start_winserv_process(ByVal p1 As String, ByVal str_prt As String) As Boolean
        Dim pr_winserv As Process = New Process
        Dim args As String = Nothing
        Dim bol_error As Boolean = True

        Select Case p1
            Case "i"
                args = String.Format("install netio -displayname NetIO-server -description ""NetIO Serverdienst"" -start auto ""{0}"" -s -p {1}",
                                     str_mydir + "\" + sNetioExe, txt_port.Text)
                logger.Info("Installing NetIO service")
            Case "c"
                args = String.Format("configure netio -displayname NetIO-server -description ""NetIO Serverdienst"" -start auto ""{0}"" -s -p {1}",
                                     str_mydir + "\" + sNetioExe, txt_port.Text)
                logger.Info("Configuring netio service")
            Case "u"
                args = "uninstall NetIO-server"
                logger.Info("Removing netio service")
        End Select
        logger.Debug("winserv arguments: " + args)

        ' winserv.exe starten
        pr_winserv.StartInfo.FileName = "winserv.exe"
        pr_winserv.StartInfo.CreateNoWindow = True
        pr_winserv.StartInfo.UseShellExecute = False
        pr_winserv.StartInfo.Arguments = args
        Try
            pr_winserv.Start()
            pr_winserv.WaitForExit()
            logger.Debug("winserv exit code: " + pr_winserv.ExitCode.ToString)
        Catch ex As Exception

        End Try

        If pr_winserv.ExitCode = 0 Then
            bol_error = False
        End If
        Return bol_error
    End Function

    Private Sub FirewallAusnahmeregelFürICMPv4PingAnlegenToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FirewallAusnahmeregelFürICMPv4PingAnlegenToolStripMenuItem.Click
        If IsAdmin() = False Then
            _elevate()
        End If
        logger.Info("Creating firewall exception for incoming ICMP packets")
        Dim answer As MsgBoxResult = MsgBox("Shall the firewall exception for incoming ICMP packets be created?", MsgBoxStyle.OkCancel)
        If answer = MsgBoxResult.Ok Then
            Dim pr_netsh As Process = New Process
            pr_netsh.StartInfo.FileName = "netsh.exe"
            pr_netsh.StartInfo.CreateNoWindow = True
            pr_netsh.StartInfo.UseShellExecute = False
            pr_netsh.StartInfo.Arguments = "advfirewall firewall add rule name=""ICMPv4 Inbound created by NetIO-GUI"" dir=in action=allow enable=yes profile=any localip=any remoteip=any protocol=icmpv4:8,any interfacetype=any edge=yes"
            logger.Debug("executing netsh.exe " + pr_netsh.StartInfo.Arguments)
            pr_netsh.Start()
            pr_netsh.WaitForExit()
            logger.Debug("netsh exit code: " + pr_netsh.ExitCode)
            If pr_netsh.ExitCode = 0 Then
                MsgBox("Successfully created firewall exception.", MsgBoxStyle.Information)
            Else
                MsgBox("An error occurred creating the firewall exception.", MsgBoxStyle.Critical)
            End If
        End If
    End Sub

    Private Sub FirewallAusnahmeregelFürnetioexeAnlegenToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FirewallAusnahmeregelFürnetioexeAnlegenToolStripMenuItem1.Click
        If IsAdmin() = False Then
            _elevate()
        End If
        logger.Info("Creating firewall exception for 'netio.exe'")
        Dim answer As MsgBoxResult = MsgBox("Shall the firewall exception for 'netio.exe' be created?", MsgBoxStyle.OkCancel)
        If answer = MsgBoxResult.Ok Then
            Dim pr_netsh As Process = New Process
            pr_netsh.StartInfo.FileName = "netsh.exe"
            pr_netsh.StartInfo.CreateNoWindow = True
            pr_netsh.StartInfo.UseShellExecute = False
            ' Netsh Advfirewall Firewall add Rule Name = "My Application" Dir = in Aktion = program="C:\MyApp\MyApp.exe zulassen" aktivieren = Ja
            pr_netsh.StartInfo.Arguments = String.Format("Advfirewall Firewall add rule name = ""netio.exe created by NetIO-GUI"" dir=in action=allow enable=yes profile=any program = ""{0}""", Path.Combine(str_mydir, sNetioExe))
            logger.Debug("executing netsh.exe " + pr_netsh.StartInfo.Arguments)
            pr_netsh.Start()
            pr_netsh.WaitForExit()
            logger.Debug("netsh exit code: " + pr_netsh.ExitCode)
            If pr_netsh.ExitCode = 0 Then
                MsgBox("Successfully created firewall exception.", MsgBoxStyle.Information)
            Else
                MsgBox("An error occurred creating the firewall exception.", MsgBoxStyle.Critical)
            End If
        End If
    End Sub

    Private Sub AlsDienstinstallierenToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AlsDienstinstallierenToolStripMenuItem1.Click
        If IsAdmin() = False Then
            _elevate()
        End If

        Dim answer As MsgBoxResult = MsgBox(String.Format("NetIO will be installed as service. {0}{0}Selected TCP/IP port: {1}", Environment.NewLine, txt_port.Text), MsgBoxStyle.OkCancel)
        If answer = MsgBoxResult.Ok Then
            If start_winserv_process("i", txt_port.Text) = False Then
                MsgBox("The service was successfully installed.", MsgBoxStyle.Information)
            Else
                MsgBox("An error occurred installing the service.", MsgBoxStyle.Critical)
            End If
        End If
    End Sub

    Private Sub ServerDienstkonfigurierenToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ServerDienstkonfigurierenToolStripMenuItem.Click

        If IsAdmin() = False Then
            _elevate()
        End If

        Dim answer As MsgBoxResult = MsgBox(String.Format("The netio service will be reconfigured. " +
                                                          "The port will be set to the actual setting.{0}{0}" +
                                                          "New port for the netio service: {1}.", Environment.NewLine,
                                                          txt_port.Text), MsgBoxStyle.OkCancel)
        If answer = MsgBoxResult.Ok Then
            If IsAdmin() = False Then
                RestartElevated()
            End If

            If start_winserv_process("c", txt_port.Text) = False Then
                _controlservice("NetIO-server", False)
                _controlservice("NetIO-server", True)
                MsgBox("The service was successfully reconfigured and restarted.", MsgBoxStyle.Information)
            Else
                MsgBox("An error ocurred reconfiguring the service.", MsgBoxStyle.Critical)
            End If
        End If
    End Sub

    Private Sub DienstdeinstallierenToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DienstdeinstallierenToolStripMenuItem1.Click
        If IsAdmin() = False Then
            _elevate()
        End If

        Dim answer As MsgBoxResult = MsgBox("The netio service will be uninstalled.", MsgBoxStyle.OkCancel)
        If answer = MsgBoxResult.Ok Then
            _controlservice("NetIO-server", False)
            If start_winserv_process("u", txt_port.Text) = False Then
                MsgBox("The netio service was successfully uninstalled.", MsgBoxStyle.Information)
            Else
                MsgBox("An error occurred uninstalling the netio service.", MsgBoxStyle.Critical)
            End If
        End If
    End Sub

    Private Sub EinstellungenToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles EinstellungenToolStripMenuItem.Click
        ' Werte aus INI lesen und in Dialogbox anzeigen
        Dim ir As IniReader = New IniReader(str_inifile)
        Settings.txt_dbfile.Text = ir.ReadString("Settings", "databasefile", str_dbfilename)
        Settings.txt_Port.Text = ir.ReadString("Settings", "serverport", "18767")
        Settings.chb_Autostart.Checked = ir.ReadBoolean("Settings", "autostartserver", False)
        Settings.txMacvendorapikey.Text = ir.ReadString("Settings", "macvendorsapikey", "Click left to get a key")

        If ir.ReadString("Settings", "protocol", "tcp") = "tcp" Then
            Settings.rb_tcp.Checked = True
        Else
            Settings.rb_udp.Checked = True
        End If
        Settings.chb_Autostart.Checked = ir.ReadBoolean("Settings", "autostartserver")
        Settings.cb_Startmode.Text = ir.ReadString("Settings", "startupmode", "Client")

        ' Neue Einstellungen in INI speichern, Variablen neu setzen, GUI auffrischen
        If Settings.ShowDialog = Windows.Forms.DialogResult.OK Then
            ir.Write("Settings", "databasefile", Settings.txt_dbfile.Text)
            ir.Write("Settings", "serverport", Settings.txt_Port.Text)
            str_dbfilename = Settings.txt_dbfile.Text
            str_port = Settings.txt_Port.Text
            ir.Write("Settings", "autostartserver", Settings.chb_Autostart.Checked)
            ir.Write("Settings", "startupmode", Settings.cb_Startmode.Text)
            ir.Write("Settings", "macvendorsapikey", Settings.txMacvendorapikey.Text)

            If Settings.rb_tcp.Checked = True Then
                rb_TCP.Checked = True
                str_protocol = "tcp"
                ir.Write("Settings", "protocol", "tcp")
            Else
                rb_UDP.Checked = True
                str_protocol = "udp"
                ir.Write("Settings", "protocol", "udp")
            End If
        End If
        Settings.Dispose()
    End Sub

    Private Sub PaypalöffnetInternetLinkToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles PaypalöffnetInternetLinkToolStripMenuItem.Click
        Dim result As MsgBoxResult
        result = MsgBox("If you use Netio-GUI on a regular basis, consider to donate a small amount of 💰. Half of the money you spend will be donated to a charity organisation like Doctors Without Frontiers or UNICEF." +
                        vbCrLf + vbCrLf + "Click Ok to open the PayPal link in your webbrowser.", MsgBoxStyle.OkCancel)
        If result = MsgBoxResult.Ok Then
            Process.Start("https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=2KUGG5BBN3FXJ")
        End If
    End Sub

    Private Sub NetIODienststartenToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles NetIODienststartenToolStripMenuItem.Click
        _controlservice("NetIO-server", True)
    End Sub

    Private Sub NetIODienstBeendenToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles NetIODienstBeendenToolStripMenuItem.Click
        _controlservice("NetIO-server", False)
    End Sub

    Private Sub toggle_mode(ByVal bol_servermode As Boolean)
        If bol_servermode = True Then
            logger.Info("activating server mode")
            txt_IP.Enabled = False
            rb_TCP.Enabled = False
            rb_UDP.Enabled = False
            btn_Start.Text = "Start server"
            cb_measure.Enabled = False
            ErgebnisseSpeichernToolStripMenuItem.Enabled = False
            grp_ResultNetIO.Enabled = False
            grp_ResultPing.Enabled = False
            cb_NetIO.Enabled = False
            cb_Ping.Enabled = False
            bol_servermode = True
            clb_IPs.Enabled = True
            set_all_ips_checked(True)
            nmRuns.Enabled = False
        Else
            logger.Info("activating client mode")
            txt_IP.Enabled = True
            rb_TCP.Enabled = True
            rb_UDP.Enabled = True
            btn_Start.Text = "Start measure"
            cb_measure.Enabled = True
            grp_ResultNetIO.Enabled = True
            grp_ResultPing.Enabled = True
            cb_NetIO.Enabled = True
            cb_Ping.Enabled = True
            bol_servermode = False
            clb_IPs.Enabled = False
            nmRuns.Enabled = True
        End If
    End Sub

    Private Sub LogdateiAnzeigenToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles LogdateiAnzeigenToolStripMenuItem.Click
        Try
            Process.Start("notepad", str_logfile)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub NetIOGUIToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles NetIOGUIToolStripMenuItem.Click
        Try
            Process.Start("notepad", str_mydir + "\gpl.txt")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub NetioToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles NetioToolStripMenuItem.Click
        Try
            Process.Start("notepad", str_mydir + "\netio\netio.doc")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub WasIstNeuTextdateiToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles WasIstNeuTextdateiToolStripMenuItem.Click
        Dim str_filename As String = "whatsnew.en.txt"
        Try
            Process.Start("notepad", str_mydir + "\" + str_filename)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub GermanToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GermanToolStripMenuItem.Click
        If File.Exists(Path.Combine(str_mydir, "NetIO-GUI.chm")) Then
            Try
                Process.Start(str_mydir + "\NetIO-GUI.chm")
            Catch ex As Exception
                MsgBox("Manual not found.", MsgBoxStyle.Information)
            End Try
        End If
    End Sub

    Private Sub EnglishToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles EnglishToolStripMenuItem.Click
        If File.Exists(Path.Combine(str_mydir, "NetIO-GUI_En.pdf")) Then
            Try
                Process.Start(Path.Combine(str_mydir, "NetIO-GUI_En.pdf"))
            Catch ex As Exception
                MsgBox("Manual not found.", MsgBoxStyle.Information)
            End Try
        End If
    End Sub

    ''' <summary>
    ''' Lookup MAC vendor from macvendors.com
    ''' </summary>
    ''' <param name="MacAddress"></param>
    ''' <returns>MAC vendor</returns>
    Private Function LookupMacVendor(MacAddress As String) As String
        Dim client = New RestClient("https://api.macvendors.com/v1/lookup/" + MacAddress)
        Dim ir As IniReader = New IniReader(str_inifile)
        Dim apikey = ir.ReadString("macvendorsapikey")
        If apikey = "" Then
            Return "You have to get a free personal API key from https://macvendors.com/"
            Exit Function
        End If
        Dim request = New RestRequest(Method.[Get])
        request.AddHeader("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiIsImp0aSI6ImE1NmE3ZTRjLTkxNjEtNDlmZC05YjA0LWNmYWVlOWJjNGUwNCJ9.eyJpc3MiOiJtYWN2ZW5kb3JzIiwiYXVkIjoibWFjdmVuZG9ycyIsImp0aSI6ImE1NmE3ZTRjLTkxNjEtNDlmZC05YjA0LWNmYWVlOWJjNGUwNCIsImlhdCI6MTY2NzQ3ODQzMywiZXhwIjoxOTgxOTc0NDMzLCJzdWIiOiIxMDQyOSIsInR5cCI6ImFjY2VzcyJ9.08QmvoK3wweXu_LVA9a4QiUJ3W0c-Qv-xIdtaw4ovW_AhIBMOTEaSyRkNiywrJpEspzL77j_DM02cBGM_K6J_Q")
        Dim response As RestResponse = client.Execute(request)
        Return response.Content
    End Function

    Private Sub NICInformationToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles NICInformationToolStripMenuItem.Click
        Dim firstMacAddress As String = NetworkInterface.GetAllNetworkInterfaces().Where(Function(nic) nic.OperationalStatus = OperationalStatus.Up AndAlso nic.NetworkInterfaceType <> NetworkInterfaceType.Loopback).[Select](Function(nic) nic.GetPhysicalAddress().ToString()).FirstOrDefault()
        'Dim firstMacAddress As String = GetMacAddress()
        If firstMacAddress <> "" Then
            Dim result = LookupMacVendor(firstMacAddress)
            If result.Contains("https") Then
                MsgBox(result, MsgBoxStyle.Information)
                Exit Sub
            Else
                MsgBox("NIC vendor from http://api.macvendors.com/ for MAC address " + firstMacAddress + ": " + vbCrLf + vbCrLf + result, MsgBoxStyle.Information)
            End If
        Else
            MsgBox("Invalid MAC address.", MsgBoxStyle.Information)
        End If
    End Sub

    ''' <summary>
    ''' Alternative method to lookup first MAC address
    ''' </summary>
    ''' <returns>MAC address of first NIC</returns>
    Private Function GetMacAddress() As String
        Dim macAddresses As String = String.Empty

        For Each nic As NetworkInterface In NetworkInterface.GetAllNetworkInterfaces()

            If nic.OperationalStatus = OperationalStatus.Up Then
                macAddresses += nic.GetPhysicalAddress().ToString()
                Exit For
            End If
        Next

        Return macAddresses
    End Function
End Class

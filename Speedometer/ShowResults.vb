﻿Imports System.Data.SQLite
Imports NLog

Public Class ShowResults
    Private logger As Logger = LogManager.GetCurrentClassLogger()
    Private ReadOnly currentCulture As String = Form1.str_currentCulture

    Private Sub ShowResults_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        Text += " " + Form1.str_dbfilename
        logger.Debug("Loading Results")
        get_Local_IPs()
        get_Remote_IPs()

        Text = "Results for host " + Form1.txt_IP.Text + " - DB-File: " + Form1.str_dbfilename
        loadresults(String.Format("SELECT [Local_Host], [Remote_Host], [Timestamp], [Protocol], [TX_1k], [TX_2k], [TX_4k], [TX_8k], [TX_16k], [TX_32k], [RX_1k], [RX_2k], [RX_4k], [RX_8k], [RX_16k], " +
                                  "[RX_32k], [TX_AVG], [RX_AVG], [Ping_AVG], [Comment], [Rating] FROM [results] WHERE [Remote_Host] = '{0}'", Form1.txt_IP.Text))
        DataGridView1.Refresh()
    End Sub

    Private Sub ShowResults_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles MyBase.FormClosed
        Dispose()
    End Sub

    Private Sub btn_ShowAll_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_ShowAll.Click
        loadresults("SELECT [Local_Host], [Remote_Host], [Timestamp], [Protocol], [TX_1k], [TX_2k], [TX_4k], [TX_8k], [TX_16k], [TX_32k], [RX_1k], [RX_2k], [RX_4k], [RX_8k], [RX_16k], [RX_32k], " +
                    "[TX_AVG], [RX_AVG], [Ping_AVG], [Comment], [Rating] FROM [results]")
        DataGridView1.Refresh()
        Text = "All results"
    End Sub

    Private Sub btn_LocalHostfilter_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_LocalHostfilter.Click
        loadresults(String.Format("SELECT [Local_Host], [Remote_Host], [Timestamp], [Protocol], [TX_1k], [TX_2k], [TX_4k], [TX_8k], [TX_16k], [TX_32k], [RX_1k], [RX_2k], [RX_4k], [RX_8k], [RX_16k], " +
                                  "[RX_32k], [TX_AVG], [RX_AVG], [Ping_AVG], [Comment] FROM [results], [Rating] WHERE [Local_Host] = '{0}'", cb_LocalIPs.Text))
        DataGridView1.Refresh()
        cb_RemoteIPs.Text = ""
        Text = "Results for host "
    End Sub

    Private Sub btn_RemoteHostfilter_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_RemoteHostfilter.Click
        loadresults(String.Format("SELECT [Local_Host], [Remote_Host], [Timestamp], [Protocol], [TX_1k], [TX_2k], [TX_4k], [TX_8k], [TX_16k], [TX_32k], [RX_1k], [RX_2k], [RX_4k], [RX_8k], [RX_16k], " +
                                  "[RX_32k], [TX_AVG], [RX_AVG], [Ping_AVG], [Comment] FROM [results], [Rating] WHERE [Remote_Host] = '{0}'", cb_RemoteIPs.Text))
        DataGridView1.Refresh()
        cb_LocalIPs.Text = ""
        Text = "Results for host "
    End Sub

    Private Sub loadresults(ByVal SQLcommand As String)
        logger.Debug("Displaying results for: " + SQLcommand)
        Dim SQLconnect As New SQLite.SQLiteConnection()

        SQLconnect.ConnectionString = "Data Source=" + Form1.str_dbfilename + ";"
        SQLconnect.Open()

        Dim da = New SQLiteDataAdapter(SQLcommand, SQLconnect)
        Dim cb As SQLiteCommandBuilder = New SQLiteCommandBuilder(da)
        Dim dt = New DataTable
        DataGridView1.DataSource = BindingSource1
        Try
            da.Fill(dt)
        Catch ex As Exception
            MsgBox("An error occurred binding database to datagrid.", MsgBoxStyle.Exclamation)
            logger.Error("error binding database to datagrid: " + ex.Message)
        End Try

        BindingSource1.DataSource = dt
    End Sub

    Private Sub get_Local_IPs()
        Dim SQLconnect As New SQLiteConnection()
        Dim SQLcommand As SQLiteCommand

        SQLconnect.ConnectionString = "Data Source=" + Form1.str_dbfilename + ";"
        SQLconnect.Open()
        SQLcommand = SQLconnect.CreateCommand
        SQLcommand.CommandText = String.Format("SELECT * FROM 'local_ips'")
        logger.Debug("Getting Local IPs SQLcommand: " + SQLcommand.CommandText)

        Dim SQLreader As SQLiteDataReader = SQLcommand.ExecuteReader()

        While SQLreader.Read()
            cb_LocalIPs.Items.Add(String.Format(SQLreader(0)))
        End While
    End Sub

    Private Sub get_Remote_IPs()
        Dim SQLconnect As New SQLite.SQLiteConnection()
        Dim SQLcommand As SQLiteCommand

        SQLconnect.ConnectionString = "Data Source=" + Form1.str_dbfilename + ";"
        SQLconnect.Open()
        SQLcommand = SQLconnect.CreateCommand
        SQLcommand.CommandText = String.Format("SELECT * FROM 'remote_ips'")
        logger.Debug("Getting Remote IPs SQLcommand: " + SQLcommand.CommandText)

        Dim SQLreader As SQLiteDataReader = SQLcommand.ExecuteReader()

        While SQLreader.Read()
            cb_RemoteIPs.Items.Add(String.Format(SQLreader(0)))
        End While
    End Sub

    Private Sub ExportAlsCSVDateiToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ExportAlsCSVDateiToolStripMenuItem.Click
        'verfying the datagridview having data or not
        If ((DataGridView1.Columns.Count = 0) Or (DataGridView1.Rows.Count = 0)) Then
            MsgBox("No data displayed for export.", MsgBoxStyle.Information)
            Exit Sub
        End If

        With SaveFileDialog1
            .Filter = "CSV files (*.csv)|*.csv"
            .RestoreDirectory = True
            If .ShowDialog() = DialogResult.OK Then
                Dim dt As DataTable = BindingSource1.DataSource
                logger.Info("Exporting results to CSV file " + .FileName)
                WriteDataTable(.FileName, dt, ";")
            End If
        End With
    End Sub

    Private Sub FensterschließenToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles FensterschließenToolStripMenuItem1.Click
        Close()
    End Sub

End Class

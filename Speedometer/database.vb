﻿Imports NLog
Imports System.Data.SQLite
Imports System.IO

Module database
    Private logger As Logger = LogManager.GetCurrentClassLogger()

    Public Sub sqlite_createdatabase(ByVal str_dbname As String)
        
        Try
            Dim SQLconnect As New SQLiteConnection()
            Dim SQLcommand As SQLiteCommand
            ' Dateiname der DataSource übergeben
            SQLconnect.ConnectionString = "Data Source=" & str_dbname & ";"

            ' Datenbankdatei erstellen
            SQLconnect.Open()
            SQLconnect.Close()

            ' Tabelle results erzeugen
            SQLconnect.Open()
            SQLcommand = SQLconnect.CreateCommand
            logger.Info("Creating table 'results'")
            SQLcommand.CommandText = "DROP TABLE IF EXISTS 'results';"
            logger.Debug("SQLcommand: " & SQLcommand.CommandText)
            SQLcommand.ExecuteNonQuery()
            SQLcommand.CommandText = "CREATE TABLE [results] (" & _
                                    "[ID] INTEGER  PRIMARY KEY NOT NULL," & _
                                    "[Local_Host] VARCHAR(15) NOT NULL," & _
                                    "[Remote_Host] VARCHAR(15) NOT NULL," & _
                                    "[Timestamp] DATETIME  NOT NULL," & _
                                    "[Protocol] VARCHAR(3)  NULL," & _
                                    "[TX_1k] NUMERIC  NULL," & _
                                    "[TX_2k] NUMERIC  NULL," & _
                                    "[TX_4k] NUMERIC  NULL," & _
                                    "[TX_8k] NUMERIC  NULL," & _
                                    "[TX_16k] NUMERIC  NULL," & _
                                    "[TX_32k] NUMERIC  NULL," & _
                                    "[RX_1K] NUMERIC  NULL," & _
                                    "[RX_2K] NUMERIC  NULL," & _
                                    "[RX_4K] NUMERIC  NULL," & _
                                    "[RX_8K] NUMERIC  NULL," & _
                                    "[RX_16K] NUMERIC  NULL," & _
                                    "[RX_32K] NUMERIC  NULL," & _
                                    "[TX_AVG] NUMERIC  NULL," & _
                                    "[RX_AVG] NUMERIC  NULL," & _
                                    "[Ping_AVG] NUMERIC  NULL," & _
                                    "[Comment] VARCHAR(140)  NULL," & _
                                    "[Rating] INTEGER  NULL" & _
                                    ");"
            logger.Debug("SQLcommand: " & SQLcommand.CommandText)
            Try
                SQLcommand.ExecuteNonQuery()
            Catch ex As Exception
                logger.Fatal("Error executing SQLcommand: " & ex.Message)
            End Try

            logger.Info("Creating table 'local_ips'")
            logger.Debug("SQLcommand: " & SQLcommand.CommandText)
            SQLcommand.CommandText = "DROP TABLE IF EXISTS 'local_ips';"
            SQLcommand.ExecuteNonQuery()
            SQLcommand.CommandText = "CREATE TABLE [local_ips] ([Local_IP] VARCHAR(15) UNIQUE NOT NULL);"
            Try
                SQLcommand.ExecuteNonQuery()
            Catch ex As Exception
                logger.Fatal("Error executing SQLcommand: " & ex.Message)
            End Try

            logger.Info("Creating table 'remote_ips'")
            logger.Debug("SQLcommand: " & SQLcommand.CommandText)
            SQLcommand.CommandText = "DROP TABLE IF EXISTS 'remote_ips';"
            SQLcommand.ExecuteNonQuery()
            SQLcommand.CommandText = "CREATE TABLE [remote_ips] ([Remote_IP] VARCHAR(15) UNIQUE NOT NULL);"
            Try
                SQLcommand.ExecuteNonQuery()
            Catch ex As Exception
                logger.Fatal("Error executing SQLcommand: " & ex.Message)
            End Try

            SQLcommand.Dispose()
        Catch ex As Exception
            logger.Fatal(ex.Message)
            MsgBox("Error creating database file: " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub

    'Public Sub sqlite_addcolumn(ByVal str_dbname As String, ByVal str_table As String, ByVal str_column As String, ByVal str_column_def As String)
    '    Try
    '        Dim SQLconnect As New Data.SQLite.SQLiteConnection()
    '        Dim SQLcommand As SQLiteCommand
    '        ' Dateiname der DataSource übergeben
    '        SQLconnect.ConnectionString = "Data Source=" & str_dbname & ";"

    '        ' Tabelle results erzeugen
    '        SQLconnect.Open()
    '        SQLcommand = SQLconnect.CreateCommand
    '        ' ALTER TABLE [results] ADD COLUMN [Rating] NUMERIC NULL;
    '        SQLcommand.CommandText = String.Format("ALTER TABLE [{0}] ADD COLUMN [{1}] {2} NULL;", str_table, str_column, str_column_def)
    '        logger.Debug("SQLcommand: " & SQLcommand.CommandText)

    '        Try
    '            SQLcommand.ExecuteNonQuery()
    '        Catch ex As Exception
    '            logger.Fatal("Error executing SQLcommand: " & ex.Message)
    '        End Try
    '        SQLcommand.Dispose()
    '    Catch ex As Exception
    '        logger.Fatal(ex.Message)
    '    End Try
    'End Sub

End Module

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.bgw_netio = New System.ComponentModel.BackgroundWorker()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.nmRuns = New System.Windows.Forms.NumericUpDown()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.rb_Client = New System.Windows.Forms.RadioButton()
        Me.rb_Server = New System.Windows.Forms.RadioButton()
        Me.clb_IPs = New System.Windows.Forms.CheckedListBox()
        Me.grp_Peer = New System.Windows.Forms.GroupBox()
        Me.txt_IP = New System.Windows.Forms.TextBox()
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar()
        Me.btn_Start = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.grp_ResultNetIO = New System.Windows.Forms.GroupBox()
        Me.lbl_avg_rx = New System.Windows.Forms.Label()
        Me.lbl_avg_tx = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.lbl_netio_32k_RX = New System.Windows.Forms.Label()
        Me.lbl_netio_16k_RX = New System.Windows.Forms.Label()
        Me.lbl_netio_8k_RX = New System.Windows.Forms.Label()
        Me.lbl_netio_4k_RX = New System.Windows.Forms.Label()
        Me.lbl_netio_2k_RX = New System.Windows.Forms.Label()
        Me.lbl_netio_1k_RX = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.lbl_netio_32k_TX = New System.Windows.Forms.Label()
        Me.lbl_netio_16k_TX = New System.Windows.Forms.Label()
        Me.lbl_netio_8k_TX = New System.Windows.Forms.Label()
        Me.lbl_netio_4k_TX = New System.Windows.Forms.Label()
        Me.lbl_netio_2k_TX = New System.Windows.Forms.Label()
        Me.lbl_netio_1k_TX = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.grp_Settings = New System.Windows.Forms.GroupBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.cb_NetIO = New System.Windows.Forms.CheckBox()
        Me.cb_Ping = New System.Windows.Forms.CheckBox()
        Me.txt_port = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.cb_measure = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.rb_UDP = New System.Windows.Forms.RadioButton()
        Me.rb_TCP = New System.Windows.Forms.RadioButton()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel3 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel2 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel5 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel4 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.DateiToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EinstellungenToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LetzteErgebnissevergleichenToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ErgebnisseSpeichernToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LogdateiAnzeigenToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BeendenToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExtrasToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.FirewallEinstellungenToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FirewallAusnahmeregelFürICMPv4PingAnlegenToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FirewallAusnahmeregelFürnetioexeAnlegenToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.NetIOServerdienstToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AlsDienstinstallierenToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ServerDienstkonfigurierenToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DienstdeinstallierenToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.NetIODienststartenToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NetIODienstBeendenToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NICInformationToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HilfeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ÜberNetIOGUIToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LizenzToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NetIOGUIToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NetioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReadmetxtToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EnglishToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GermanToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SpendenToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PaypalöffnetInternetLinkToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.WasIstNeuTextdateiToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.grp_ResultPing = New System.Windows.Forms.GroupBox()
        Me.lbl_avg_ping = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.lbl_ping_1024b = New System.Windows.Forms.Label()
        Me.lbl_ping_512b = New System.Windows.Forms.Label()
        Me.lbl_ping_256b = New System.Windows.Forms.Label()
        Me.lbl_ping_128b = New System.Windows.Forms.Label()
        Me.lbl_ping_64b = New System.Windows.Forms.Label()
        Me.lbl_ping_32b = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        Me.GroupBox1.SuspendLayout()
        CType(Me.nmRuns, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grp_Peer.SuspendLayout()
        Me.grp_ResultNetIO.SuspendLayout()
        Me.grp_Settings.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        Me.grp_ResultPing.SuspendLayout()
        Me.SuspendLayout()
        '
        'bgw_netio
        '
        Me.bgw_netio.WorkerReportsProgress = True
        Me.bgw_netio.WorkerSupportsCancellation = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label14)
        Me.GroupBox1.Controls.Add(Me.nmRuns)
        Me.GroupBox1.Controls.Add(Me.PictureBox1)
        Me.GroupBox1.Controls.Add(Me.rb_Client)
        Me.GroupBox1.Controls.Add(Me.rb_Server)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 28)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(213, 91)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Mode"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(135, 63)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(32, 13)
        Me.Label14.TabIndex = 4
        Me.Label14.Text = "Runs"
        '
        'nmRuns
        '
        Me.nmRuns.Location = New System.Drawing.Point(83, 60)
        Me.nmRuns.Maximum = New Decimal(New Integer() {9999, 0, 0, 0})
        Me.nmRuns.Name = "nmRuns"
        Me.nmRuns.Size = New System.Drawing.Size(46, 20)
        Me.nmRuns.TabIndex = 3
        Me.ToolTip1.SetToolTip(Me.nmRuns, "Anzahl der Durchgänge (0 = unbegrenzt)")
        Me.nmRuns.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.Speedometer.My.Resources.Resources.Workgroup_icon1
        Me.PictureBox1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.PictureBox1.Location = New System.Drawing.Point(6, 18)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(64, 64)
        Me.PictureBox1.TabIndex = 2
        Me.PictureBox1.TabStop = False
        '
        'rb_Client
        '
        Me.rb_Client.AutoSize = True
        Me.rb_Client.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.rb_Client.Location = New System.Drawing.Point(83, 19)
        Me.rb_Client.Name = "rb_Client"
        Me.rb_Client.Size = New System.Drawing.Size(81, 17)
        Me.rb_Client.TabIndex = 1
        Me.rb_Client.Text = "Client-Mode"
        Me.rb_Client.UseVisualStyleBackColor = True
        '
        'rb_Server
        '
        Me.rb_Server.AutoSize = True
        Me.rb_Server.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.rb_Server.Location = New System.Drawing.Point(83, 42)
        Me.rb_Server.Name = "rb_Server"
        Me.rb_Server.Size = New System.Drawing.Size(86, 17)
        Me.rb_Server.TabIndex = 0
        Me.rb_Server.Text = "Server-Mode"
        Me.rb_Server.UseVisualStyleBackColor = True
        '
        'clb_IPs
        '
        Me.clb_IPs.FormattingEnabled = True
        Me.clb_IPs.Location = New System.Drawing.Point(73, 136)
        Me.clb_IPs.Name = "clb_IPs"
        Me.clb_IPs.Size = New System.Drawing.Size(120, 34)
        Me.clb_IPs.TabIndex = 6
        '
        'grp_Peer
        '
        Me.grp_Peer.Controls.Add(Me.txt_IP)
        Me.grp_Peer.Controls.Add(Me.ProgressBar1)
        Me.grp_Peer.Controls.Add(Me.btn_Start)
        Me.grp_Peer.Controls.Add(Me.Label4)
        Me.grp_Peer.Location = New System.Drawing.Point(12, 125)
        Me.grp_Peer.Name = "grp_Peer"
        Me.grp_Peer.Size = New System.Drawing.Size(213, 104)
        Me.grp_Peer.TabIndex = 0
        Me.grp_Peer.TabStop = False
        Me.grp_Peer.Text = "Peer"
        '
        'txt_IP
        '
        Me.txt_IP.Location = New System.Drawing.Point(74, 18)
        Me.txt_IP.Name = "txt_IP"
        Me.txt_IP.Size = New System.Drawing.Size(127, 20)
        Me.txt_IP.TabIndex = 4
        '
        'ProgressBar1
        '
        Me.ProgressBar1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.ProgressBar1.Location = New System.Drawing.Point(9, 73)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(192, 20)
        Me.ProgressBar1.TabIndex = 3
        '
        'btn_Start
        '
        Me.btn_Start.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btn_Start.Location = New System.Drawing.Point(74, 44)
        Me.btn_Start.Name = "btn_Start"
        Me.btn_Start.Size = New System.Drawing.Size(127, 23)
        Me.btn_Start.TabIndex = 1
        Me.btn_Start.Text = "Start Server"
        Me.btn_Start.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(6, 19)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(52, 13)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "IP-Adress"
        '
        'grp_ResultNetIO
        '
        Me.grp_ResultNetIO.Controls.Add(Me.lbl_avg_rx)
        Me.grp_ResultNetIO.Controls.Add(Me.lbl_avg_tx)
        Me.grp_ResultNetIO.Controls.Add(Me.Label16)
        Me.grp_ResultNetIO.Controls.Add(Me.lbl_netio_32k_RX)
        Me.grp_ResultNetIO.Controls.Add(Me.lbl_netio_16k_RX)
        Me.grp_ResultNetIO.Controls.Add(Me.lbl_netio_8k_RX)
        Me.grp_ResultNetIO.Controls.Add(Me.lbl_netio_4k_RX)
        Me.grp_ResultNetIO.Controls.Add(Me.lbl_netio_2k_RX)
        Me.grp_ResultNetIO.Controls.Add(Me.lbl_netio_1k_RX)
        Me.grp_ResultNetIO.Controls.Add(Me.Label17)
        Me.grp_ResultNetIO.Controls.Add(Me.lbl_netio_32k_TX)
        Me.grp_ResultNetIO.Controls.Add(Me.lbl_netio_16k_TX)
        Me.grp_ResultNetIO.Controls.Add(Me.lbl_netio_8k_TX)
        Me.grp_ResultNetIO.Controls.Add(Me.lbl_netio_4k_TX)
        Me.grp_ResultNetIO.Controls.Add(Me.lbl_netio_2k_TX)
        Me.grp_ResultNetIO.Controls.Add(Me.lbl_netio_1k_TX)
        Me.grp_ResultNetIO.Controls.Add(Me.Label10)
        Me.grp_ResultNetIO.Controls.Add(Me.Label9)
        Me.grp_ResultNetIO.Controls.Add(Me.Label8)
        Me.grp_ResultNetIO.Controls.Add(Me.Label7)
        Me.grp_ResultNetIO.Controls.Add(Me.Label6)
        Me.grp_ResultNetIO.Controls.Add(Me.Label5)
        Me.grp_ResultNetIO.Controls.Add(Me.Label2)
        Me.grp_ResultNetIO.Controls.Add(Me.Label1)
        Me.grp_ResultNetIO.Location = New System.Drawing.Point(231, 235)
        Me.grp_ResultNetIO.Name = "grp_ResultNetIO"
        Me.grp_ResultNetIO.Size = New System.Drawing.Size(213, 165)
        Me.grp_ResultNetIO.TabIndex = 3
        Me.grp_ResultNetIO.TabStop = False
        Me.grp_ResultNetIO.Text = "Results NetIO"
        '
        'lbl_avg_rx
        '
        Me.lbl_avg_rx.AutoSize = True
        Me.lbl_avg_rx.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lbl_avg_rx.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lbl_avg_rx.Location = New System.Drawing.Point(147, 145)
        Me.lbl_avg_rx.Name = "lbl_avg_rx"
        Me.lbl_avg_rx.Size = New System.Drawing.Size(11, 13)
        Me.lbl_avg_rx.TabIndex = 23
        Me.lbl_avg_rx.Text = "-"
        '
        'lbl_avg_tx
        '
        Me.lbl_avg_tx.AutoSize = True
        Me.lbl_avg_tx.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lbl_avg_tx.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lbl_avg_tx.Location = New System.Drawing.Point(80, 145)
        Me.lbl_avg_tx.Name = "lbl_avg_tx"
        Me.lbl_avg_tx.Size = New System.Drawing.Size(11, 13)
        Me.lbl_avg_tx.TabIndex = 22
        Me.lbl_avg_tx.Text = "-"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label16.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label16.Location = New System.Drawing.Point(6, 145)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(16, 13)
        Me.Label16.TabIndex = 21
        Me.Label16.Text = "Ø"
        '
        'lbl_netio_32k_RX
        '
        Me.lbl_netio_32k_RX.AutoSize = True
        Me.lbl_netio_32k_RX.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lbl_netio_32k_RX.Location = New System.Drawing.Point(147, 125)
        Me.lbl_netio_32k_RX.Name = "lbl_netio_32k_RX"
        Me.lbl_netio_32k_RX.Size = New System.Drawing.Size(10, 13)
        Me.lbl_netio_32k_RX.TabIndex = 20
        Me.lbl_netio_32k_RX.Text = "-"
        '
        'lbl_netio_16k_RX
        '
        Me.lbl_netio_16k_RX.AutoSize = True
        Me.lbl_netio_16k_RX.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lbl_netio_16k_RX.Location = New System.Drawing.Point(147, 108)
        Me.lbl_netio_16k_RX.Name = "lbl_netio_16k_RX"
        Me.lbl_netio_16k_RX.Size = New System.Drawing.Size(10, 13)
        Me.lbl_netio_16k_RX.TabIndex = 19
        Me.lbl_netio_16k_RX.Text = "-"
        '
        'lbl_netio_8k_RX
        '
        Me.lbl_netio_8k_RX.AutoSize = True
        Me.lbl_netio_8k_RX.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lbl_netio_8k_RX.Location = New System.Drawing.Point(147, 91)
        Me.lbl_netio_8k_RX.Name = "lbl_netio_8k_RX"
        Me.lbl_netio_8k_RX.Size = New System.Drawing.Size(10, 13)
        Me.lbl_netio_8k_RX.TabIndex = 18
        Me.lbl_netio_8k_RX.Text = "-"
        '
        'lbl_netio_4k_RX
        '
        Me.lbl_netio_4k_RX.AutoSize = True
        Me.lbl_netio_4k_RX.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lbl_netio_4k_RX.Location = New System.Drawing.Point(147, 74)
        Me.lbl_netio_4k_RX.Name = "lbl_netio_4k_RX"
        Me.lbl_netio_4k_RX.Size = New System.Drawing.Size(10, 13)
        Me.lbl_netio_4k_RX.TabIndex = 17
        Me.lbl_netio_4k_RX.Text = "-"
        '
        'lbl_netio_2k_RX
        '
        Me.lbl_netio_2k_RX.AutoSize = True
        Me.lbl_netio_2k_RX.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lbl_netio_2k_RX.Location = New System.Drawing.Point(147, 57)
        Me.lbl_netio_2k_RX.Name = "lbl_netio_2k_RX"
        Me.lbl_netio_2k_RX.Size = New System.Drawing.Size(10, 13)
        Me.lbl_netio_2k_RX.TabIndex = 16
        Me.lbl_netio_2k_RX.Text = "-"
        '
        'lbl_netio_1k_RX
        '
        Me.lbl_netio_1k_RX.AutoSize = True
        Me.lbl_netio_1k_RX.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lbl_netio_1k_RX.Location = New System.Drawing.Point(147, 40)
        Me.lbl_netio_1k_RX.Name = "lbl_netio_1k_RX"
        Me.lbl_netio_1k_RX.Size = New System.Drawing.Size(10, 13)
        Me.lbl_netio_1k_RX.TabIndex = 15
        Me.lbl_netio_1k_RX.Text = "-"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label17.Location = New System.Drawing.Point(145, 16)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(37, 13)
        Me.Label17.TabIndex = 14
        Me.Label17.Text = "RX (↓)"
        '
        'lbl_netio_32k_TX
        '
        Me.lbl_netio_32k_TX.AutoSize = True
        Me.lbl_netio_32k_TX.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lbl_netio_32k_TX.Location = New System.Drawing.Point(80, 125)
        Me.lbl_netio_32k_TX.Name = "lbl_netio_32k_TX"
        Me.lbl_netio_32k_TX.Size = New System.Drawing.Size(10, 13)
        Me.lbl_netio_32k_TX.TabIndex = 13
        Me.lbl_netio_32k_TX.Text = "-"
        '
        'lbl_netio_16k_TX
        '
        Me.lbl_netio_16k_TX.AutoSize = True
        Me.lbl_netio_16k_TX.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lbl_netio_16k_TX.Location = New System.Drawing.Point(80, 108)
        Me.lbl_netio_16k_TX.Name = "lbl_netio_16k_TX"
        Me.lbl_netio_16k_TX.Size = New System.Drawing.Size(10, 13)
        Me.lbl_netio_16k_TX.TabIndex = 12
        Me.lbl_netio_16k_TX.Text = "-"
        '
        'lbl_netio_8k_TX
        '
        Me.lbl_netio_8k_TX.AutoSize = True
        Me.lbl_netio_8k_TX.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lbl_netio_8k_TX.Location = New System.Drawing.Point(80, 91)
        Me.lbl_netio_8k_TX.Name = "lbl_netio_8k_TX"
        Me.lbl_netio_8k_TX.Size = New System.Drawing.Size(10, 13)
        Me.lbl_netio_8k_TX.TabIndex = 11
        Me.lbl_netio_8k_TX.Text = "-"
        '
        'lbl_netio_4k_TX
        '
        Me.lbl_netio_4k_TX.AutoSize = True
        Me.lbl_netio_4k_TX.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lbl_netio_4k_TX.Location = New System.Drawing.Point(80, 74)
        Me.lbl_netio_4k_TX.Name = "lbl_netio_4k_TX"
        Me.lbl_netio_4k_TX.Size = New System.Drawing.Size(10, 13)
        Me.lbl_netio_4k_TX.TabIndex = 10
        Me.lbl_netio_4k_TX.Text = "-"
        '
        'lbl_netio_2k_TX
        '
        Me.lbl_netio_2k_TX.AutoSize = True
        Me.lbl_netio_2k_TX.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lbl_netio_2k_TX.Location = New System.Drawing.Point(80, 57)
        Me.lbl_netio_2k_TX.Name = "lbl_netio_2k_TX"
        Me.lbl_netio_2k_TX.Size = New System.Drawing.Size(10, 13)
        Me.lbl_netio_2k_TX.TabIndex = 9
        Me.lbl_netio_2k_TX.Text = "-"
        '
        'lbl_netio_1k_TX
        '
        Me.lbl_netio_1k_TX.AutoSize = True
        Me.lbl_netio_1k_TX.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lbl_netio_1k_TX.Location = New System.Drawing.Point(80, 40)
        Me.lbl_netio_1k_TX.Name = "lbl_netio_1k_TX"
        Me.lbl_netio_1k_TX.Size = New System.Drawing.Size(10, 13)
        Me.lbl_netio_1k_TX.TabIndex = 8
        Me.lbl_netio_1k_TX.Text = "-"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label10.Location = New System.Drawing.Point(6, 124)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(25, 13)
        Me.Label10.TabIndex = 7
        Me.Label10.Text = "32k"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label9.Location = New System.Drawing.Point(6, 107)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(25, 13)
        Me.Label9.TabIndex = 6
        Me.Label9.Text = "16k"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label8.Location = New System.Drawing.Point(6, 90)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(19, 13)
        Me.Label8.TabIndex = 5
        Me.Label8.Text = "8k"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(6, 73)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(19, 13)
        Me.Label7.TabIndex = 4
        Me.Label7.Text = "4k"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(6, 56)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(19, 13)
        Me.Label6.TabIndex = 3
        Me.Label6.Text = "2k"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(6, 39)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(19, 13)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "1k"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(78, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(36, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "TX (↑)"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(6, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(62, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Packet size"
        '
        'grp_Settings
        '
        Me.grp_Settings.Controls.Add(Me.clb_IPs)
        Me.grp_Settings.Controls.Add(Me.Label13)
        Me.grp_Settings.Controls.Add(Me.cb_NetIO)
        Me.grp_Settings.Controls.Add(Me.cb_Ping)
        Me.grp_Settings.Controls.Add(Me.txt_port)
        Me.grp_Settings.Controls.Add(Me.Label12)
        Me.grp_Settings.Controls.Add(Me.Label11)
        Me.grp_Settings.Controls.Add(Me.cb_measure)
        Me.grp_Settings.Controls.Add(Me.Label3)
        Me.grp_Settings.Controls.Add(Me.rb_UDP)
        Me.grp_Settings.Controls.Add(Me.rb_TCP)
        Me.grp_Settings.Location = New System.Drawing.Point(231, 28)
        Me.grp_Settings.Name = "grp_Settings"
        Me.grp_Settings.Size = New System.Drawing.Size(213, 201)
        Me.grp_Settings.TabIndex = 2
        Me.grp_Settings.TabStop = False
        Me.grp_Settings.Text = "Settings"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label13.Location = New System.Drawing.Point(6, 115)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(33, 13)
        Me.Label13.TabIndex = 10
        Me.Label13.Text = "Tests"
        '
        'cb_NetIO
        '
        Me.cb_NetIO.AutoSize = True
        Me.cb_NetIO.Checked = True
        Me.cb_NetIO.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cb_NetIO.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cb_NetIO.Location = New System.Drawing.Point(147, 113)
        Me.cb_NetIO.Name = "cb_NetIO"
        Me.cb_NetIO.Size = New System.Drawing.Size(54, 17)
        Me.cb_NetIO.TabIndex = 5
        Me.cb_NetIO.Text = "NetIO"
        Me.cb_NetIO.UseVisualStyleBackColor = True
        '
        'cb_Ping
        '
        Me.cb_Ping.AutoSize = True
        Me.cb_Ping.Checked = True
        Me.cb_Ping.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cb_Ping.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cb_Ping.Location = New System.Drawing.Point(73, 113)
        Me.cb_Ping.Name = "cb_Ping"
        Me.cb_Ping.Size = New System.Drawing.Size(47, 17)
        Me.cb_Ping.TabIndex = 4
        Me.cb_Ping.Text = "Ping"
        Me.cb_Ping.UseVisualStyleBackColor = True
        '
        'txt_port
        '
        Me.txt_port.Location = New System.Drawing.Point(74, 87)
        Me.txt_port.Name = "txt_port"
        Me.txt_port.Size = New System.Drawing.Size(127, 20)
        Me.txt_port.TabIndex = 3
        Me.txt_port.Text = "18767"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label12.Location = New System.Drawing.Point(6, 89)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(26, 13)
        Me.Label12.TabIndex = 9
        Me.Label12.Text = "Port"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label11.Location = New System.Drawing.Point(6, 63)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(26, 13)
        Me.Label11.TabIndex = 8
        Me.Label11.Text = "Unit"
        '
        'cb_measure
        '
        Me.cb_measure.FormattingEnabled = True
        Me.cb_measure.Items.AddRange(New Object() {"Bytes/Sec", "Kilobytes/Sec", "Megabytes/Sec", "Gigabytes/Sec"})
        Me.cb_measure.Location = New System.Drawing.Point(74, 60)
        Me.cb_measure.Name = "cb_measure"
        Me.cb_measure.Size = New System.Drawing.Size(127, 21)
        Me.cb_measure.TabIndex = 2
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(6, 16)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(46, 13)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Protocol"
        '
        'rb_UDP
        '
        Me.rb_UDP.AutoSize = True
        Me.rb_UDP.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.rb_UDP.Location = New System.Drawing.Point(74, 37)
        Me.rb_UDP.Name = "rb_UDP"
        Me.rb_UDP.Size = New System.Drawing.Size(48, 17)
        Me.rb_UDP.TabIndex = 1
        Me.rb_UDP.Text = "UDP"
        Me.rb_UDP.UseVisualStyleBackColor = True
        '
        'rb_TCP
        '
        Me.rb_TCP.AutoSize = True
        Me.rb_TCP.Checked = True
        Me.rb_TCP.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.rb_TCP.Location = New System.Drawing.Point(74, 14)
        Me.rb_TCP.Name = "rb_TCP"
        Me.rb_TCP.Size = New System.Drawing.Size(46, 17)
        Me.rb_TCP.TabIndex = 0
        Me.rb_TCP.TabStop = True
        Me.rb_TCP.Text = "TCP"
        Me.rb_TCP.UseVisualStyleBackColor = True
        '
        'StatusStrip1
        '
        Me.StatusStrip1.ImageScalingSize = New System.Drawing.Size(24, 24)
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1, Me.ToolStripStatusLabel3, Me.ToolStripStatusLabel2, Me.ToolStripStatusLabel5, Me.ToolStripStatusLabel4})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 414)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(459, 22)
        Me.StatusStrip1.SizingGrip = False
        Me.StatusStrip1.TabIndex = 24
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(119, 17)
        Me.ToolStripStatusLabel1.Text = "ToolStripStatusLabel1"
        '
        'ToolStripStatusLabel3
        '
        Me.ToolStripStatusLabel3.Name = "ToolStripStatusLabel3"
        Me.ToolStripStatusLabel3.Size = New System.Drawing.Size(16, 17)
        Me.ToolStripStatusLabel3.Text = " | "
        '
        'ToolStripStatusLabel2
        '
        Me.ToolStripStatusLabel2.Name = "ToolStripStatusLabel2"
        Me.ToolStripStatusLabel2.Size = New System.Drawing.Size(119, 17)
        Me.ToolStripStatusLabel2.Text = "ToolStripStatusLabel2"
        '
        'ToolStripStatusLabel5
        '
        Me.ToolStripStatusLabel5.Name = "ToolStripStatusLabel5"
        Me.ToolStripStatusLabel5.Size = New System.Drawing.Size(16, 17)
        Me.ToolStripStatusLabel5.Text = " | "
        '
        'ToolStripStatusLabel4
        '
        Me.ToolStripStatusLabel4.Name = "ToolStripStatusLabel4"
        Me.ToolStripStatusLabel4.Size = New System.Drawing.Size(119, 17)
        Me.ToolStripStatusLabel4.Text = "ToolStripStatusLabel4"
        '
        'MenuStrip1
        '
        Me.MenuStrip1.ImageScalingSize = New System.Drawing.Size(24, 24)
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DateiToolStripMenuItem, Me.ExtrasToolStripMenuItem1, Me.HilfeToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(459, 24)
        Me.MenuStrip1.TabIndex = 25
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'DateiToolStripMenuItem
        '
        Me.DateiToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.EinstellungenToolStripMenuItem, Me.LetzteErgebnissevergleichenToolStripMenuItem, Me.ErgebnisseSpeichernToolStripMenuItem, Me.LogdateiAnzeigenToolStripMenuItem, Me.BeendenToolStripMenuItem})
        Me.DateiToolStripMenuItem.Name = "DateiToolStripMenuItem"
        Me.DateiToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.DateiToolStripMenuItem.Text = "&File"
        '
        'EinstellungenToolStripMenuItem
        '
        Me.EinstellungenToolStripMenuItem.Name = "EinstellungenToolStripMenuItem"
        Me.EinstellungenToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.EinstellungenToolStripMenuItem.Text = "&Settings"
        '
        'LetzteErgebnissevergleichenToolStripMenuItem
        '
        Me.LetzteErgebnissevergleichenToolStripMenuItem.Name = "LetzteErgebnissevergleichenToolStripMenuItem"
        Me.LetzteErgebnissevergleichenToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.LetzteErgebnissevergleichenToolStripMenuItem.Text = "Result &database"
        '
        'ErgebnisseSpeichernToolStripMenuItem
        '
        Me.ErgebnisseSpeichernToolStripMenuItem.Name = "ErgebnisseSpeichernToolStripMenuItem"
        Me.ErgebnisseSpeichernToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.ErgebnisseSpeichernToolStripMenuItem.Text = "&Save results (Text)"
        '
        'LogdateiAnzeigenToolStripMenuItem
        '
        Me.LogdateiAnzeigenToolStripMenuItem.Name = "LogdateiAnzeigenToolStripMenuItem"
        Me.LogdateiAnzeigenToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.LogdateiAnzeigenToolStripMenuItem.Text = "Show &log file"
        '
        'BeendenToolStripMenuItem
        '
        Me.BeendenToolStripMenuItem.Name = "BeendenToolStripMenuItem"
        Me.BeendenToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.BeendenToolStripMenuItem.Text = "&Exit"
        '
        'ExtrasToolStripMenuItem1
        '
        Me.ExtrasToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FirewallEinstellungenToolStripMenuItem, Me.NetIOServerdienstToolStripMenuItem, Me.NICInformationToolStripMenuItem})
        Me.ExtrasToolStripMenuItem1.Name = "ExtrasToolStripMenuItem1"
        Me.ExtrasToolStripMenuItem1.Size = New System.Drawing.Size(45, 20)
        Me.ExtrasToolStripMenuItem1.Text = "&Extra"
        '
        'FirewallEinstellungenToolStripMenuItem
        '
        Me.FirewallEinstellungenToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FirewallAusnahmeregelFürICMPv4PingAnlegenToolStripMenuItem, Me.FirewallAusnahmeregelFürnetioexeAnlegenToolStripMenuItem1})
        Me.FirewallEinstellungenToolStripMenuItem.Name = "FirewallEinstellungenToolStripMenuItem"
        Me.FirewallEinstellungenToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.FirewallEinstellungenToolStripMenuItem.Text = "&Firewall Settings"
        '
        'FirewallAusnahmeregelFürICMPv4PingAnlegenToolStripMenuItem
        '
        Me.FirewallAusnahmeregelFürICMPv4PingAnlegenToolStripMenuItem.Image = Global.Speedometer.My.Resources.Resources.Shield16
        Me.FirewallAusnahmeregelFürICMPv4PingAnlegenToolStripMenuItem.Name = "FirewallAusnahmeregelFürICMPv4PingAnlegenToolStripMenuItem"
        Me.FirewallAusnahmeregelFürICMPv4PingAnlegenToolStripMenuItem.Size = New System.Drawing.Size(266, 22)
        Me.FirewallAusnahmeregelFürICMPv4PingAnlegenToolStripMenuItem.Text = "Firewall exception for ICMPv4 (&Ping)"
        '
        'FirewallAusnahmeregelFürnetioexeAnlegenToolStripMenuItem1
        '
        Me.FirewallAusnahmeregelFürnetioexeAnlegenToolStripMenuItem1.Image = Global.Speedometer.My.Resources.Resources.Shield16
        Me.FirewallAusnahmeregelFürnetioexeAnlegenToolStripMenuItem1.Name = "FirewallAusnahmeregelFürnetioexeAnlegenToolStripMenuItem1"
        Me.FirewallAusnahmeregelFürnetioexeAnlegenToolStripMenuItem1.Size = New System.Drawing.Size(266, 22)
        Me.FirewallAusnahmeregelFürnetioexeAnlegenToolStripMenuItem1.Text = "Firewall exception for '&netio.exe'"
        '
        'NetIOServerdienstToolStripMenuItem
        '
        Me.NetIOServerdienstToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AlsDienstinstallierenToolStripMenuItem1, Me.ServerDienstkonfigurierenToolStripMenuItem, Me.DienstdeinstallierenToolStripMenuItem1, Me.NetIODienststartenToolStripMenuItem, Me.NetIODienstBeendenToolStripMenuItem})
        Me.NetIOServerdienstToolStripMenuItem.Name = "NetIOServerdienstToolStripMenuItem"
        Me.NetIOServerdienstToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.NetIOServerdienstToolStripMenuItem.Text = "&NetIO service"
        '
        'AlsDienstinstallierenToolStripMenuItem1
        '
        Me.AlsDienstinstallierenToolStripMenuItem1.Image = Global.Speedometer.My.Resources.Resources.Shield16
        Me.AlsDienstinstallierenToolStripMenuItem1.Name = "AlsDienstinstallierenToolStripMenuItem1"
        Me.AlsDienstinstallierenToolStripMenuItem1.Size = New System.Drawing.Size(196, 22)
        Me.AlsDienstinstallierenToolStripMenuItem1.Text = "&Install netio as service"
        '
        'ServerDienstkonfigurierenToolStripMenuItem
        '
        Me.ServerDienstkonfigurierenToolStripMenuItem.Image = Global.Speedometer.My.Resources.Resources.Shield16
        Me.ServerDienstkonfigurierenToolStripMenuItem.Name = "ServerDienstkonfigurierenToolStripMenuItem"
        Me.ServerDienstkonfigurierenToolStripMenuItem.Size = New System.Drawing.Size(196, 22)
        Me.ServerDienstkonfigurierenToolStripMenuItem.Text = "&Configure netio service"
        '
        'DienstdeinstallierenToolStripMenuItem1
        '
        Me.DienstdeinstallierenToolStripMenuItem1.Image = Global.Speedometer.My.Resources.Resources.Shield16
        Me.DienstdeinstallierenToolStripMenuItem1.Name = "DienstdeinstallierenToolStripMenuItem1"
        Me.DienstdeinstallierenToolStripMenuItem1.Size = New System.Drawing.Size(196, 22)
        Me.DienstdeinstallierenToolStripMenuItem1.Text = "&Uninstall netio service"
        '
        'NetIODienststartenToolStripMenuItem
        '
        Me.NetIODienststartenToolStripMenuItem.Image = Global.Speedometer.My.Resources.Resources.Shield16
        Me.NetIODienststartenToolStripMenuItem.Name = "NetIODienststartenToolStripMenuItem"
        Me.NetIODienststartenToolStripMenuItem.Size = New System.Drawing.Size(196, 22)
        Me.NetIODienststartenToolStripMenuItem.Text = "&Start netio service"
        '
        'NetIODienstBeendenToolStripMenuItem
        '
        Me.NetIODienstBeendenToolStripMenuItem.Image = Global.Speedometer.My.Resources.Resources.Shield16
        Me.NetIODienstBeendenToolStripMenuItem.Name = "NetIODienstBeendenToolStripMenuItem"
        Me.NetIODienstBeendenToolStripMenuItem.Size = New System.Drawing.Size(196, 22)
        Me.NetIODienstBeendenToolStripMenuItem.Text = "Sto&p netio service"
        '
        'NICInformationToolStripMenuItem
        '
        Me.NICInformationToolStripMenuItem.Enabled = False
        Me.NICInformationToolStripMenuItem.Name = "NICInformationToolStripMenuItem"
        Me.NICInformationToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.NICInformationToolStripMenuItem.Text = "NIC &Information"
        '
        'HilfeToolStripMenuItem
        '
        Me.HilfeToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ÜberNetIOGUIToolStripMenuItem, Me.LizenzToolStripMenuItem, Me.ReadmetxtToolStripMenuItem, Me.SpendenToolStripMenuItem, Me.WasIstNeuTextdateiToolStripMenuItem})
        Me.HilfeToolStripMenuItem.Name = "HilfeToolStripMenuItem"
        Me.HilfeToolStripMenuItem.Size = New System.Drawing.Size(44, 20)
        Me.HilfeToolStripMenuItem.Text = "&Help"
        '
        'ÜberNetIOGUIToolStripMenuItem
        '
        Me.ÜberNetIOGUIToolStripMenuItem.Name = "ÜberNetIOGUIToolStripMenuItem"
        Me.ÜberNetIOGUIToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.ÜberNetIOGUIToolStripMenuItem.Text = "About NetIO-&GUI"
        '
        'LizenzToolStripMenuItem
        '
        Me.LizenzToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.NetIOGUIToolStripMenuItem, Me.NetioToolStripMenuItem})
        Me.LizenzToolStripMenuItem.Name = "LizenzToolStripMenuItem"
        Me.LizenzToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.LizenzToolStripMenuItem.Text = "&Licenses"
        '
        'NetIOGUIToolStripMenuItem
        '
        Me.NetIOGUIToolStripMenuItem.Name = "NetIOGUIToolStripMenuItem"
        Me.NetIOGUIToolStripMenuItem.Size = New System.Drawing.Size(129, 22)
        Me.NetIOGUIToolStripMenuItem.Text = "NetIO-GUI"
        '
        'NetioToolStripMenuItem
        '
        Me.NetioToolStripMenuItem.Name = "NetioToolStripMenuItem"
        Me.NetioToolStripMenuItem.Size = New System.Drawing.Size(129, 22)
        Me.NetioToolStripMenuItem.Text = "netio"
        '
        'ReadmetxtToolStripMenuItem
        '
        Me.ReadmetxtToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.EnglishToolStripMenuItem, Me.GermanToolStripMenuItem})
        Me.ReadmetxtToolStripMenuItem.Name = "ReadmetxtToolStripMenuItem"
        Me.ReadmetxtToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.ReadmetxtToolStripMenuItem.Text = "&Manual"
        '
        'EnglishToolStripMenuItem
        '
        Me.EnglishToolStripMenuItem.Name = "EnglishToolStripMenuItem"
        Me.EnglishToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.EnglishToolStripMenuItem.Text = "&English"
        '
        'GermanToolStripMenuItem
        '
        Me.GermanToolStripMenuItem.Name = "GermanToolStripMenuItem"
        Me.GermanToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.GermanToolStripMenuItem.Text = "&German"
        '
        'SpendenToolStripMenuItem
        '
        Me.SpendenToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PaypalöffnetInternetLinkToolStripMenuItem})
        Me.SpendenToolStripMenuItem.Name = "SpendenToolStripMenuItem"
        Me.SpendenToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.SpendenToolStripMenuItem.Text = "&Donate"
        '
        'PaypalöffnetInternetLinkToolStripMenuItem
        '
        Me.PaypalöffnetInternetLinkToolStripMenuItem.Name = "PaypalöffnetInternetLinkToolStripMenuItem"
        Me.PaypalöffnetInternetLinkToolStripMenuItem.Size = New System.Drawing.Size(109, 22)
        Me.PaypalöffnetInternetLinkToolStripMenuItem.Text = "&Paypal"
        '
        'WasIstNeuTextdateiToolStripMenuItem
        '
        Me.WasIstNeuTextdateiToolStripMenuItem.Name = "WasIstNeuTextdateiToolStripMenuItem"
        Me.WasIstNeuTextdateiToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.WasIstNeuTextdateiToolStripMenuItem.Text = "&What´s new?"
        '
        'grp_ResultPing
        '
        Me.grp_ResultPing.Controls.Add(Me.lbl_avg_ping)
        Me.grp_ResultPing.Controls.Add(Me.Label19)
        Me.grp_ResultPing.Controls.Add(Me.lbl_ping_1024b)
        Me.grp_ResultPing.Controls.Add(Me.lbl_ping_512b)
        Me.grp_ResultPing.Controls.Add(Me.lbl_ping_256b)
        Me.grp_ResultPing.Controls.Add(Me.lbl_ping_128b)
        Me.grp_ResultPing.Controls.Add(Me.lbl_ping_64b)
        Me.grp_ResultPing.Controls.Add(Me.lbl_ping_32b)
        Me.grp_ResultPing.Controls.Add(Me.Label27)
        Me.grp_ResultPing.Controls.Add(Me.Label28)
        Me.grp_ResultPing.Controls.Add(Me.Label29)
        Me.grp_ResultPing.Controls.Add(Me.Label30)
        Me.grp_ResultPing.Controls.Add(Me.Label31)
        Me.grp_ResultPing.Controls.Add(Me.Label32)
        Me.grp_ResultPing.Controls.Add(Me.Label33)
        Me.grp_ResultPing.Controls.Add(Me.Label34)
        Me.grp_ResultPing.Location = New System.Drawing.Point(12, 235)
        Me.grp_ResultPing.Name = "grp_ResultPing"
        Me.grp_ResultPing.Size = New System.Drawing.Size(213, 165)
        Me.grp_ResultPing.TabIndex = 24
        Me.grp_ResultPing.TabStop = False
        Me.grp_ResultPing.Text = "Results Ping [ms]"
        '
        'lbl_avg_ping
        '
        Me.lbl_avg_ping.AutoSize = True
        Me.lbl_avg_ping.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lbl_avg_ping.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lbl_avg_ping.Location = New System.Drawing.Point(80, 146)
        Me.lbl_avg_ping.Name = "lbl_avg_ping"
        Me.lbl_avg_ping.Size = New System.Drawing.Size(11, 13)
        Me.lbl_avg_ping.TabIndex = 15
        Me.lbl_avg_ping.Text = "-"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label19.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label19.Location = New System.Drawing.Point(6, 145)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(16, 13)
        Me.Label19.TabIndex = 14
        Me.Label19.Text = "Ø"
        '
        'lbl_ping_1024b
        '
        Me.lbl_ping_1024b.AutoSize = True
        Me.lbl_ping_1024b.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lbl_ping_1024b.Location = New System.Drawing.Point(80, 125)
        Me.lbl_ping_1024b.Name = "lbl_ping_1024b"
        Me.lbl_ping_1024b.Size = New System.Drawing.Size(10, 13)
        Me.lbl_ping_1024b.TabIndex = 13
        Me.lbl_ping_1024b.Text = "-"
        '
        'lbl_ping_512b
        '
        Me.lbl_ping_512b.AutoSize = True
        Me.lbl_ping_512b.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lbl_ping_512b.Location = New System.Drawing.Point(80, 108)
        Me.lbl_ping_512b.Name = "lbl_ping_512b"
        Me.lbl_ping_512b.Size = New System.Drawing.Size(10, 13)
        Me.lbl_ping_512b.TabIndex = 12
        Me.lbl_ping_512b.Text = "-"
        '
        'lbl_ping_256b
        '
        Me.lbl_ping_256b.AutoSize = True
        Me.lbl_ping_256b.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lbl_ping_256b.Location = New System.Drawing.Point(80, 91)
        Me.lbl_ping_256b.Name = "lbl_ping_256b"
        Me.lbl_ping_256b.Size = New System.Drawing.Size(10, 13)
        Me.lbl_ping_256b.TabIndex = 11
        Me.lbl_ping_256b.Text = "-"
        '
        'lbl_ping_128b
        '
        Me.lbl_ping_128b.AutoSize = True
        Me.lbl_ping_128b.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lbl_ping_128b.Location = New System.Drawing.Point(80, 74)
        Me.lbl_ping_128b.Name = "lbl_ping_128b"
        Me.lbl_ping_128b.Size = New System.Drawing.Size(10, 13)
        Me.lbl_ping_128b.TabIndex = 10
        Me.lbl_ping_128b.Text = "-"
        '
        'lbl_ping_64b
        '
        Me.lbl_ping_64b.AutoSize = True
        Me.lbl_ping_64b.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lbl_ping_64b.Location = New System.Drawing.Point(80, 57)
        Me.lbl_ping_64b.Name = "lbl_ping_64b"
        Me.lbl_ping_64b.Size = New System.Drawing.Size(10, 13)
        Me.lbl_ping_64b.TabIndex = 9
        Me.lbl_ping_64b.Text = "-"
        '
        'lbl_ping_32b
        '
        Me.lbl_ping_32b.AutoSize = True
        Me.lbl_ping_32b.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lbl_ping_32b.Location = New System.Drawing.Point(80, 40)
        Me.lbl_ping_32b.Name = "lbl_ping_32b"
        Me.lbl_ping_32b.Size = New System.Drawing.Size(10, 13)
        Me.lbl_ping_32b.TabIndex = 8
        Me.lbl_ping_32b.Text = "-"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label27.Location = New System.Drawing.Point(6, 124)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(37, 13)
        Me.Label27.TabIndex = 7
        Me.Label27.Text = "1024b"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label28.Location = New System.Drawing.Point(6, 107)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(31, 13)
        Me.Label28.TabIndex = 6
        Me.Label28.Text = "512b"
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label29.Location = New System.Drawing.Point(6, 90)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(31, 13)
        Me.Label29.TabIndex = 5
        Me.Label29.Text = "256b"
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label30.Location = New System.Drawing.Point(6, 73)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(31, 13)
        Me.Label30.TabIndex = 4
        Me.Label30.Text = "128b"
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label31.Location = New System.Drawing.Point(6, 56)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(25, 13)
        Me.Label31.TabIndex = 3
        Me.Label31.Text = "64b"
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label32.Location = New System.Drawing.Point(6, 39)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(25, 13)
        Me.Label32.TabIndex = 2
        Me.Label32.Text = "32b"
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label33.Location = New System.Drawing.Point(78, 16)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(79, 13)
        Me.Label33.TabIndex = 1
        Me.Label33.Text = "Roundtrip Time"
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label34.Location = New System.Drawing.Point(6, 16)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(62, 13)
        Me.Label34.TabIndex = 0
        Me.Label34.Text = "Packet size"
        '
        'Form1
        '
        Me.AcceptButton = Me.btn_Start
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(459, 436)
        Me.Controls.Add(Me.grp_ResultPing)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.grp_Settings)
        Me.Controls.Add(Me.grp_ResultNetIO)
        Me.Controls.Add(Me.grp_Peer)
        Me.Controls.Add(Me.GroupBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.MaximizeBox = False
        Me.Name = "Form1"
        Me.Text = "NetIO-GUI"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.nmRuns, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grp_Peer.ResumeLayout(False)
        Me.grp_Peer.PerformLayout()
        Me.grp_ResultNetIO.ResumeLayout(False)
        Me.grp_ResultNetIO.PerformLayout()
        Me.grp_Settings.ResumeLayout(False)
        Me.grp_Settings.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.grp_ResultPing.ResumeLayout(False)
        Me.grp_ResultPing.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents bgw_netio As System.ComponentModel.BackgroundWorker
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents rb_Client As System.Windows.Forms.RadioButton
    Friend WithEvents rb_Server As System.Windows.Forms.RadioButton
    Friend WithEvents grp_Peer As System.Windows.Forms.GroupBox
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents btn_Start As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents grp_ResultNetIO As System.Windows.Forms.GroupBox
    Friend WithEvents lbl_netio_32k_RX As System.Windows.Forms.Label
    Friend WithEvents lbl_netio_16k_RX As System.Windows.Forms.Label
    Friend WithEvents lbl_netio_8k_RX As System.Windows.Forms.Label
    Friend WithEvents lbl_netio_4k_RX As System.Windows.Forms.Label
    Friend WithEvents lbl_netio_2k_RX As System.Windows.Forms.Label
    Friend WithEvents lbl_netio_1k_RX As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents lbl_netio_32k_TX As System.Windows.Forms.Label
    Friend WithEvents lbl_netio_16k_TX As System.Windows.Forms.Label
    Friend WithEvents lbl_netio_8k_TX As System.Windows.Forms.Label
    Friend WithEvents lbl_netio_4k_TX As System.Windows.Forms.Label
    Friend WithEvents lbl_netio_2k_TX As System.Windows.Forms.Label
    Friend WithEvents lbl_netio_1k_TX As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents grp_Settings As System.Windows.Forms.GroupBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents cb_NetIO As System.Windows.Forms.CheckBox
    Friend WithEvents cb_Ping As System.Windows.Forms.CheckBox
    Friend WithEvents txt_port As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents cb_measure As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents rb_UDP As System.Windows.Forms.RadioButton
    Friend WithEvents rb_TCP As System.Windows.Forms.RadioButton
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents ToolStripStatusLabel2 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel3 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents clb_IPs As System.Windows.Forms.CheckedListBox
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents DateiToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ErgebnisseSpeichernToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BeendenToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HilfeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ÜberNetIOGUIToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReadmetxtToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LetzteErgebnissevergleichenToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lbl_avg_rx As System.Windows.Forms.Label
    Friend WithEvents lbl_avg_tx As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents ExtrasToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FirewallEinstellungenToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FirewallAusnahmeregelFürICMPv4PingAnlegenToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FirewallAusnahmeregelFürnetioexeAnlegenToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NetIOServerdienstToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AlsDienstinstallierenToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ServerDienstkonfigurierenToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DienstdeinstallierenToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripStatusLabel4 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel5 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents EinstellungenToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SpendenToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PaypalöffnetInternetLinkToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NetIODienststartenToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NetIODienstBeendenToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LogdateiAnzeigenToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LizenzToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NetIOGUIToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NetioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents WasIstNeuTextdateiToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents grp_ResultPing As System.Windows.Forms.GroupBox
    Friend WithEvents lbl_avg_ping As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents lbl_ping_1024b As System.Windows.Forms.Label
    Friend WithEvents lbl_ping_512b As System.Windows.Forms.Label
    Friend WithEvents lbl_ping_256b As System.Windows.Forms.Label
    Friend WithEvents lbl_ping_128b As System.Windows.Forms.Label
    Friend WithEvents lbl_ping_64b As System.Windows.Forms.Label
    Friend WithEvents lbl_ping_32b As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents nmRuns As System.Windows.Forms.NumericUpDown
    Friend WithEvents EnglishToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents GermanToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents NICInformationToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents txt_IP As TextBox
End Class

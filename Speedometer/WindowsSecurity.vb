﻿Imports System.Security.Principal
Imports NLog

Module WindowsSecurity
    Private logger As Logger = LogManager.GetCurrentClassLogger()
    'Declare API
    Private Declare Ansi Function SendMessage Lib "user32.dll" Alias "SendMessageA" (ByVal hwnd As Integer, ByVal wMsg As Integer, ByVal wParam As Integer, ByVal lParam As String) As Integer
    Private Const BCM_FIRST As Int32 = &H1600
    Private Const BCM_SETSHIELD As Int32 = (BCM_FIRST + &HC)

    Public Function IsVistaOrHigher() As Boolean
        Return Environment.OSVersion.Version.Major < 6
    End Function

    ' Checks if the process is elevated
    Public Function IsAdmin() As Boolean
        Dim id As WindowsIdentity = WindowsIdentity.GetCurrent()
        Dim p As WindowsPrincipal = New WindowsPrincipal(id)
        Return p.IsInRole(WindowsBuiltInRole.Administrator)
    End Function

    ' Add a shield icon to a button
    Public Sub AddShieldToButton(ByRef b As Button)
        b.FlatStyle = FlatStyle.System
        SendMessage(b.Handle, BCM_SETSHIELD, 0, &HFFFFFFFF)
    End Sub

    ' Restart the current process with administrator credentials
    Public Sub RestartElevated()
        Dim startInfo As ProcessStartInfo = New ProcessStartInfo()
        startInfo.UseShellExecute = True
        startInfo.WorkingDirectory = Environment.CurrentDirectory
        startInfo.FileName = Application.ExecutablePath
        startInfo.Verb = "runas"
        Try
            Dim p As Process = Process.Start(startInfo)
        Catch ex As Exception
            Return 'If cancelled, do nothing
        End Try
        Application.Exit()
    End Sub

    Public Sub _elevate()
        Dim answer As MsgBoxResult = MsgBox("Note: This function requires elevated rights. NetIO-GUI will now restart and request administrator rights. Afterwards please call the function again.", MsgBoxStyle.OkCancel)
        If answer = MsgBoxResult.Ok Then
            logger.Info("Restarting elevated")
            RestartElevated()
        End If
    End Sub

End Module

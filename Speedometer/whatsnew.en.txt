﻿v 1.0.4
	prevent error when trying to start a non-supported language -> fallback to en-US
	Menu Help - License(s) expanded: NetIO-GUI and netio (new)
	New Menu: Help - What´s new
	Export result database as Excel- or CSV file
	Rate your result on a scale from 1 to 5
	Setup now stops and restart netio server service
	Better detection of "installed" mode

v 1.0.6
	Removed buggy localization code (English only from now)
	Reoved buggy support for Microsoft Excel export (CSV only)
	Updated netio.exe to version 1.32
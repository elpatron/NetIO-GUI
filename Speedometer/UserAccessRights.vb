﻿'===============================================================================
'= Anwendungsbeispiel ==========================================================
'===============================================================================

'Private Sub Button1_Click(ByVal sender As System.Object, _
'                          ByVal e As System.EventArgs) Handles Button1.Click
'   Dim UAR As New UserAccessRights("C:\Windows\")

'   For Each ur In UAR.GetAllowedRights.Split(";"c)
'      ListBox1.Items.Add(ur.Trim)
'   Next
'End Sub


Imports System.Security.Principal
Imports System.Security.AccessControl

''' <summary>
''' Stellt Eigenschaften und Methoden für die Benutzerrechte eines Pfades bereit.
''' </summary>
''' <remarks></remarks>
Public Class UserAccessRights

#Region " Deklarationen - Felder "

    Private pFileInfo As IO.FileInfo
    Private pRights As Generic.Dictionary(Of String, AccessRight)
    Private pUser As WindowsIdentity

#End Region

#Region " Konstruktoren "

    ''' <summary>
    ''' Initialisiert eine neue Instanz der UserAccessRights-Klasse für den angegebenen Pfad des aktuellen Benutzers.
    ''' </summary>
    ''' <param name="path">Der Pfad dessen Berechtigungen ermittelt werden sollen.</param>
    ''' <exception cref="System.IO.FileNotFoundException">Die Datei kann nicht gefunden werden.</exception>
    ''' <exception cref="System.IO.DirectoryNotFoundException">Das Verzeichnis kann nicht gefunden werden.</exception>
    ''' <exception cref="System.ArgumentNullException">fileName ist null.</exception>
    ''' <exception cref="System.Security.SecurityException">Der Aufrufer verfügt nicht über die erforderliche Berechtigung.</exception>
    ''' <exception cref="System.ArgumentException">Der Dateiname ist leer, oder er enthält nur Leerräume oder ungültige Zeichen.</exception>
    ''' <exception cref="System.UnauthorizedAccessException">Der Zugriff auf fileName wird verweigert.</exception>
    ''' <exception cref="System.IO.PathTooLongException">Der angegebene Pfad und/oder der Dateiname überschreiten die vom System vorgegebene Höchstlänge. Beispielsweise dürfen auf Windows-Plattformen Pfade nicht länger als 247 Zeichen und Dateinamen nicht länger als 259 Zeichen sein.</exception>
    ''' <exception cref="System.NotSupportedException">fileName enthält einen Doppelpunkt (:) innerhalb der Zeichenfolge.</exception>
    ''' <remarks></remarks>
    Public Sub New(ByVal path As String)
        ' Prüfe, ob Datei/Verzeichnis vorhanden
        If Not IO.File.Exists(path) AndAlso Not IO.Directory.Exists(path) Then
            ' nicht vorhanden -> Prüfe, ob Dateierweiterung fehlt
            If String.IsNullOrEmpty(IO.Path.GetExtension(path)) Then
                ' true -> Verzeichnis existiert nicht
                Throw New System.IO.DirectoryNotFoundException
            Else
                ' false -> Datei existiert nicht
                Throw New System.IO.FileNotFoundException
            End If
        End If

        pFileInfo = New IO.FileInfo(path)
        pUser = WindowsIdentity.GetCurrent

        pRights = New Dictionary(Of String, AccessRight)

        ' Iteriere durch jede Berechtigung und füge diese dem Dictionary hinzu
        For Each r As String In [Enum].GetNames(GetType(FileSystemRights))
            pRights.Add(r, New AccessRight)
        Next

        ' Intialisiere die Benutzerrechte für den Pfad
        Call InitializeRights()
    End Sub

#End Region

#Region " Eigenschaften "

    ''' <summary>
    ''' Gibt die Berechtigung an, Daten an das Ende einer Datei anzufügen.
    ''' </summary>
    ''' <value></value>
    ''' <returns>true, wenn der Benutzer diese Berechtigung besitzt, ansonsten false.</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property CanAppendData() As Boolean
        Get
            Return Not pRights(FileSystemRights.AppendData.ToString).Deny AndAlso _
                   pRights(FileSystemRights.AppendData.ToString).Allow
        End Get
    End Property

    ''' <summary>
    ''' Gibt die Berechtigung an, die einer Datei zugeordneten Sicherheits- und Überwachungsregeln zu
    ''' ändern.
    ''' </summary>
    ''' <value></value>
    ''' <returns>true, wenn der Benutzer diese Berechtigung besitzt, ansonsten false.</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property CanChangePermissions() As Boolean
        Get
            Return Not pRights(FileSystemRights.ChangePermissions.ToString).Deny AndAlso _
                   pRights(FileSystemRights.ChangePermissions.ToString).Allow
        End Get
    End Property

    ''' <summary>
    ''' Gibt die Berechtigung an, einen Ordner zu erstellen.
    ''' </summary>
    ''' <value></value>
    ''' <returns>true, wenn der Benutzer diese Berechtigung besitzt, ansonsten false.</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property CanCreateDirectories() As Boolean
        Get
            Return Not pRights(FileSystemRights.CreateDirectories.ToString).Deny AndAlso _
                   pRights(FileSystemRights.CreateDirectories.ToString).Allow
        End Get
    End Property

    ''' <summary>
    ''' Gibt die Berechtigung an, eine Datei zu erstellen.
    ''' </summary>
    ''' <value></value>
    ''' <returns>true, wenn der Benutzer diese Berechtigung besitzt, ansonsten false.</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property CanCreateFiles() As Boolean
        Get
            Return Not pRights(FileSystemRights.CreateFiles.ToString).Deny AndAlso _
                   pRights(FileSystemRights.CreateFiles.ToString).Allow
        End Get
    End Property

    ''' <summary>
    ''' Gibt die Berechtigung an, einen Ordner oder eine Datei zu löschen.
    ''' </summary>
    ''' <value></value>
    ''' <returns>true, wenn der Benutzer diese Berechtigung besitzt, ansonsten false.</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property CanDelete() As Boolean
        Get
            Return Not pRights(FileSystemRights.Delete.ToString).Deny AndAlso _
                   pRights(FileSystemRights.Delete.ToString).Allow
        End Get
    End Property

    ''' <summary>
    ''' Gibt die Berechtigung an, einen Ordner und sämtliche in diesem Ordner enthaltenen Dateien zu
    ''' löschen.
    ''' </summary>
    ''' <value></value>
    ''' <returns>true, wenn der Benutzer diese Berechtigung besitzt, ansonsten false.</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property CanDeleteSubdirectoriesAndFiles() As Boolean
        Get
            Return Not pRights(FileSystemRights.DeleteSubdirectoriesAndFiles.ToString).Deny AndAlso _
                   pRights(FileSystemRights.DeleteSubdirectoriesAndFiles.ToString).Allow
        End Get
    End Property

    ''' <summary>
    ''' Gibt die Berechtigung an, eine Anwendungsdatei auszuführen.
    ''' </summary>
    ''' <value></value>
    ''' <returns>true, wenn der Benutzer diese Berechtigung besitzt, ansonsten false.</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property CanExecuteFile() As Boolean
        Get
            Return Not pRights(FileSystemRights.ExecuteFile.ToString).Deny AndAlso _
                   pRights(FileSystemRights.ExecuteFile.ToString).Allow
        End Get
    End Property

    ''' <summary>
    ''' Gibt die Berechtigung für einen Vollzugriff auf eine Datei oder einen Ordner an sowie die
    ''' Berechtigung, die Zugriffs- und Überwachungsregeln zu ändern. Dieser Wert stellt die Berechtigung
    ''' dar, jede mögliche Aktion für diese Datei durchzuführen. Er ist eine Kombination aller
    ''' Berechtigungen.
    ''' </summary>
    ''' <value></value>
    ''' <returns>true, wenn der Benutzer diese Berechtigung besitzt, ansonsten false.</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property CanFullControl() As Boolean
        Get
            Return Not pRights(FileSystemRights.FullControl.ToString).Deny AndAlso _
                   pRights(FileSystemRights.FullControl.ToString).Allow
        End Get
    End Property

    ''' <summary>
    ''' Gibt die Berechtigung an, den Inhalt eines Verzeichnisses zu lesen.
    ''' </summary>
    ''' <value></value>
    ''' <returns>true, wenn der Benutzer diese Berechtigung besitzt, ansonsten false.</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property CanListDirectory() As Boolean
        Get
            Return Not pRights(FileSystemRights.ListDirectory.ToString).Deny AndAlso _
                   pRights(FileSystemRights.ListDirectory.ToString).Allow
        End Get
    End Property

    ''' <summary>
    ''' Gibt die Berechtigung an, den Inhalt eines Ordners zu lesen, zu schreiben und aufzulisten,
    ''' Dateien und Ordner zu löschen und Anwendungsdateien auszuführen. Diese Berechtigung schließt
    ''' die Berechtigungen CanReadAndExecute, CanWrite und CanDelete ein.
    ''' </summary>
    ''' <value></value>
    ''' <returns>true, wenn der Benutzer diese Berechtigung besitzt, ansonsten false.</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property CanModify() As Boolean
        Get
            Return Not pRights(FileSystemRights.Modify.ToString).Deny AndAlso _
                   pRights(FileSystemRights.Modify.ToString).Allow
        End Get
    End Property

    ''' <summary>
    ''' Gibt die Berechtigung an, Ordner oder Dateien schreibgeschützt zu öffnen und zu kopieren. Diese
    ''' Berechtigung schließt die Berechtigungen CanReadData, CanReadExtendedAttributes,
    ''' CanReadAttributes und CanReadPermissions ein.
    ''' </summary>
    ''' <value></value>
    ''' <returns>true, wenn der Benutzer diese Berechtigung besitzt, ansonsten false.</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property CanRead() As Boolean
        Get
            Return Not pRights(FileSystemRights.Read.ToString).Deny AndAlso _
                   pRights(FileSystemRights.Read.ToString).Allow
        End Get
    End Property

    ''' <summary>
    ''' Gibt die Berechtigung an, Ordner oder Dateien schreibgeschützt zu öffnen und zu kopieren und
    ''' Anwendungsdateien auszuführen. Diese Berechtigung schließt die CanRead-Berechtigung und die
    ''' CanExecuteFile-Berechtigung ein.
    ''' </summary>
    ''' <value></value>
    ''' <returns>true, wenn der Benutzer diese Berechtigung besitzt, ansonsten false.</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property CanReadAndExecute() As Boolean
        Get
            Return Not pRights(FileSystemRights.ReadAndExecute.ToString).Deny AndAlso _
                   pRights(FileSystemRights.ReadAndExecute.ToString).Allow
        End Get
    End Property

    ''' <summary>
    ''' Gibt die Berechtigung an, Dateisystemattribute einer Datei oder eines Ordners zu öffnen und zu
    ''' kopieren. Dieser Wert gibt z. B. die Berechtigung an, das Erstellungsdatum oder das
    ''' Änderungsdatum einer Datei zu lesen. Dies schließt nicht die Berechtigung ein, Daten, erweiterte
    ''' Dateisystemattribute oder Zugriffs- und Überwachungsregeln zu lesen.
    ''' </summary>
    ''' <value></value>
    ''' <returns>true, wenn der Benutzer diese Berechtigung besitzt, ansonsten false.</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property CanReadAttributes() As Boolean
        Get
            Return Not pRights(FileSystemRights.ReadAttributes.ToString).Deny AndAlso _
                   pRights(FileSystemRights.ReadAttributes.ToString).Allow
        End Get
    End Property

    ''' <summary>
    ''' Gibt die Berechtigung an, eine Datei oder einen Ordner zu öffnen und zu kopieren. Dies schließt
    ''' nicht die Berechtigung ein, Dateisystemattribute, erweiterte Dateisystemattribute oder Zugriffs-
    ''' und Überwachungsregeln zu lesen.
    ''' </summary>
    ''' <value></value>
    ''' <returns>true, wenn der Benutzer diese Berechtigung besitzt, ansonsten false.</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property CanReadData() As Boolean
        Get
            Return Not pRights(FileSystemRights.ReadData.ToString).Deny AndAlso _
                   pRights(FileSystemRights.ReadData.ToString).Allow
        End Get
    End Property

    ''' <summary>
    ''' Gibt die Berechtigung an, erweiterte Dateisystemattribute einer Datei oder eines Ordners zu öffnen
    ''' und zu kopieren. Dieser Wert gibt zum Beispiel die Berechtigung an, den Autor oder
    ''' Inhaltsinformationen anzuzeigen. Dies schließt nicht die Berechtigung ein, Daten,
    ''' Dateisystemattribute oder Zugriffs- und Überwachungsregeln zu lesen.
    ''' </summary>
    ''' <value></value>
    ''' <returns>true, wenn der Benutzer diese Berechtigung besitzt, ansonsten false.</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property CanReadExtendedAttributes() As Boolean
        Get
            Return Not pRights(FileSystemRights.ReadExtendedAttributes.ToString).Deny AndAlso _
                   pRights(FileSystemRights.ReadExtendedAttributes.ToString).Allow
        End Get
    End Property

    ''' <summary>
    ''' Gibt die Berechtigung an, Zugriffs- und Überwachungsregeln für eine Datei oder einen Ordner zu
    ''' öffnen und zu kopieren. Dies schließt nicht die Berechtigung ein, Daten, Dateisystemattribute
    ''' oder erweiterte Dateisystemattribute zu lesen.
    ''' </summary>
    ''' <value></value>
    ''' <returns>true, wenn der Benutzer diese Berechtigung besitzt, ansonsten false.</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property CanReadPermissions() As Boolean
        Get
            Return Not pRights(FileSystemRights.ReadPermissions.ToString).Deny AndAlso _
                   pRights(FileSystemRights.ReadPermissions.ToString).Allow
        End Get
    End Property

    ''' <summary>
    ''' Gibt an, ob die Anwendung warten kann, bis ein Dateihandle mit dem Abschluss eines E/A-Vorgangs
    ''' synchronisiert ist.
    ''' </summary>
    ''' <value></value>
    ''' <returns>true, wenn der Benutzer diese Berechtigung besitzt, ansonsten false.</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property CanSynchronize() As Boolean
        Get
            Return Not pRights(FileSystemRights.Synchronize.ToString).Deny AndAlso _
                   pRights(FileSystemRights.Synchronize.ToString).Allow
        End Get
    End Property

    ''' <summary>
    ''' Gibt die Berechtigung an, den Besitzer eines Ordners oder einer Datei zu ändern. Beachten Sie,
    ''' dass Besitzer einer Ressource über einen Vollzugriff auf diese Ressource verfügen.
    ''' </summary>
    ''' <value></value>
    ''' <returns>true, wenn der Benutzer diese Berechtigung besitzt, ansonsten false.</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property CanTakeOwnership() As Boolean
        Get
            Return Not pRights(FileSystemRights.TakeOwnership.ToString).Deny AndAlso _
                   pRights(FileSystemRights.TakeOwnership.ToString).Allow
        End Get
    End Property

    ''' <summary>
    ''' Gibt die Berechtigung an, den Inhalt eines Ordners aufzulisten und in diesem Ordner enthaltene
    ''' Anwendungen auszuführen.
    ''' </summary>
    ''' <value></value>
    ''' <returns>true, wenn der Benutzer diese Berechtigung besitzt, ansonsten false.</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property CanTraverse() As Boolean
        Get
            Return Not pRights(FileSystemRights.Traverse.ToString).Deny AndAlso _
                   pRights(FileSystemRights.Traverse.ToString).Allow
        End Get
    End Property

    ''' <summary>
    ''' Gibt die Berechtigung an, Ordner und Dateien zu erstellen, Dateien Daten hinzuzufügen und Daten
    ''' aus Dateien zu entfernen. Diese Berechtigung schließt die Berechtigungen CanWriteData,
    ''' CanAppendData, CanWriteExtendedAttributes und CanWriteAttributes ein.
    ''' </summary>
    ''' <value></value>
    ''' <returns>true, wenn der Benutzer diese Berechtigung besitzt, ansonsten false.</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property CanWrite() As Boolean
        Get
            Return Not pRights(FileSystemRights.Write.ToString).Deny AndAlso _
                   pRights(FileSystemRights.Write.ToString).Allow
        End Get
    End Property

    ''' <summary>
    ''' Gibt die Berechtigung an, Dateisystemattribute einer Datei oder eines Ordners zu öffnen und zu
    ''' schreiben. Dies schließt nicht die Berechtigung ein, Daten, erweiterte Attribute oder Zugriffs-
    ''' und Überwachungsregeln zu schreiben.
    ''' </summary>
    ''' <value></value>
    ''' <returns>true, wenn der Benutzer diese Berechtigung besitzt, ansonsten false.</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property CanWriteAttributes() As Boolean
        Get
            Return Not pRights(FileSystemRights.WriteAttributes.ToString).Deny AndAlso _
                   pRights(FileSystemRights.WriteAttributes.ToString).Allow
        End Get
    End Property

    ''' <summary>
    ''' Gibt die Berechtigung an, eine Datei oder einen Ordner zu öffnen und in die Datei bzw. den Ordner
    ''' zu schreiben. Dies schließt nicht die Berechtigung ein, Dateisystemattribute, erweiterte
    ''' Dateisystemattribute oder Zugriffs- und Überwachungsregeln zu öffnen und zu schreiben.
    ''' </summary>
    ''' <value></value>
    ''' <returns>true, wenn der Benutzer diese Berechtigung besitzt, ansonsten false.</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property CanWriteData() As Boolean
        Get
            Return Not pRights(FileSystemRights.WriteData.ToString).Deny AndAlso _
                   pRights(FileSystemRights.WriteData.ToString).Allow
        End Get
    End Property

    ''' <summary>
    ''' Gibt die Berechtigung an, erweiterte Dateisystemattribute einer Datei oder eines Ordners zu öffnen
    '''  und zu schreiben. Dies schließt nicht die Berechtigung ein, Daten, Attribute oder Zugriffs- und
    ''' Überwachungsregeln zu schreiben.
    ''' </summary>
    ''' <value></value>
    ''' <returns>true, wenn der Benutzer diese Berechtigung besitzt, ansonsten false.</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property CanWriteExtendedAttributes() As Boolean
        Get
            Return Not pRights(FileSystemRights.WriteExtendedAttributes.ToString).Deny AndAlso _
                   pRights(FileSystemRights.WriteExtendedAttributes.ToString).Allow
        End Get
    End Property

    ''' <summary>
    ''' Ruft die vollständige Zeichenfolge für den zu prüfenden Pfad ab oder legt diesen fest.
    ''' </summary>
    ''' <value></value>
    ''' <returns>Der vollständige zu prüfende Pfad.</returns>
    ''' <exception cref="System.ArgumentNullException">fileName ist null.</exception>
    ''' <exception cref="System.Security.SecurityException">Der Aufrufer verfügt nicht über die erforderliche Berechtigung.</exception>
    ''' <exception cref="System.ArgumentException">Der Dateiname ist leer, oder er enthält nur Leerräume oder ungültige Zeichen.</exception>
    ''' <exception cref="System.UnauthorizedAccessException">Der Zugriff auf fileName wird verweigert.</exception>
    ''' <exception cref="System.IO.PathTooLongException">Der angegebene Pfad und/oder der Dateiname überschreiten die vom System vorgegebene Höchstlänge. Beispielsweise dürfen auf Windows-Plattformen Pfade nicht länger als 247 Zeichen und Dateinamen nicht länger als 259  Zeichen sein.</exception>
    ''' <exception cref="System.NotSupportedException">fileName enthält einen Doppelpunkt (:) innerhalb der Zeichenfolge.</exception>
    ''' <remarks></remarks>
    Public Property Path() As String
        Get
            Return pFileInfo.FullName
        End Get
        Set(ByVal value As String)
            pFileInfo = New IO.FileInfo(value)

            ' Rechte aktualisieren
            Call InitializeRights()
        End Set
    End Property

#End Region

#Region " Methoden - Function "

    ''' <summary>
    ''' Prüft, ob die angegebene Berechtigung in der angegebenen FileSystemAccessRule vorhanden ist.
    ''' </summary>
    ''' <param name="right">Die Berechtigung als Zeichenfolge.</param>
    ''' <param name="rule">Die zu prüfende FileSystemAccessRule.</param>
    ''' <returns>true, wenn die Berechtigung vorhanden ist, ansonsten false.</returns>
    ''' <remarks></remarks>
    Private Function Contains(ByVal right As String, ByVal rule As FileSystemAccessRule) As Boolean
        ' Zeichenkette (Berechtigung) parsen
        Dim fsr As FileSystemRights = DirectCast([Enum].Parse(GetType(FileSystemRights), right),  _
                                      FileSystemRights)

        ' Prüfen, ob diese vorhanden ist
        Return (fsr And rule.FileSystemRights) = fsr
    End Function

    ''' <summary>
    ''' Gibt eine durch Semikolon getrennte Zeichenkette aller erlaubten Zugriffsrechte des Benutzers zurück.
    ''' </summary>
    ''' <returns>Eine Zeichenkette aller erlaubten Zugriffsrechte.</returns>
    ''' <remarks></remarks>
    Public Function GetAllowedRights() As String
        Return GetRights(True)
    End Function

    ''' <summary>
    ''' Gibt eine durch Semikolon getrennte Zeichenkette aller verweigerten Zugriffsrechte des Benutzers zurück.
    ''' </summary>
    ''' <returns>Eine Zeichenkette aller verweigerten Zugriffsrechte.</returns>
    ''' <remarks></remarks>
    Public Function GetDeniedRights() As String
        Return GetRights(False)
    End Function

    ''' <summary>
    ''' Gibt eine durch Semikolon getrennte Zeichenkette aller Zugriffsrechte mit dem angegebenen Status zurück.
    ''' </summary>
    ''' <param name="rightState">Der Status der zurückzugebenden Zugriffsrechte.</param>
    ''' <returns>Eine Zeichenkette aller Zugriffsrechte mit dem angegebenen Status.</returns>
    ''' <remarks></remarks>
    Private Function GetRights(ByVal rightState As Boolean) As String
        ' StringBuilder verwenden
        Dim sb As New System.Text.StringBuilder

        ' Iteriere durch alle Eigenschaften dieser Klasse
        For Each pi In Me.GetType().GetProperties()
            ' Prüfe, ob die Eigenschaft mit "Can" beginnt und lesbar ist
            If pi.Name.StartsWith("Can") AndAlso pi.CanRead Then
                ' Wert der Eigeschaft ermitteln
                Dim value As String = pi.GetValue(Me, Nothing).ToString
                Dim result As Boolean

                ' Wert der Eigenschaft in Boolean parsen
                If Boolean.TryParse(value, result) Then
                    ' Prüfe auf Übereinstimmung und füge evtl. den Name der Berechtigung hin
                    If result = rightState Then sb.Append(pi.Name.Substring(3) & "; ")
                End If
            End If
        Next

        ' Prüfe ob länger als 2 Zeichen und schneide die letzten beiden Zeichen ab (Leerzeichen und Semikolon)
        If sb.Length > 2 Then sb.Remove(sb.Length - 2, 2)

        ' Zeichenkette zurückgeben
        Return sb.ToString
    End Function

#End Region

#Region " Methoden - Sub "

    ''' <summary>
    ''' Initialisiert die Zugriffsrechte für den aktuellen Pfad des Benutzers.
    ''' </summary>
    ''' <exception cref="System.IO.IOException">E/A-Fehler beim Öffnen der Datei.</exception>
    ''' <exception cref="System.PlatformNotSupportedException">Das aktuelle Betriebssystem ist nicht Microsoft Windows 2000 oder höher.</exception>
    ''' <exception cref="System.Security.AccessControl.PrivilegeNotHeldException">Dem aktuellen Systemkonto sind keine Administratorrechte zugewiesen.</exception>
    ''' <exception cref="System.SystemException">Die Datei konnte nicht gefunden werden.</exception>
    ''' <exception cref="System.UnauthorizedAccessException">Dieser Vorgang wird von der aktuellen Plattform nicht unterstützt.- oder - Der Aufrufer verfügt nicht über die erforderliche Berechtigung.</exception>
    ''' <remarks></remarks>
    Private Sub InitializeRights()
        ' Prüfe, ob Benutzer Nothing ist
        If pUser.User Is Nothing Then Exit Sub
        If String.IsNullOrEmpty(Me.Path) Then Call ResetRights() : Exit Sub

        ' Ermittle alle AuthorizationRule-Objekte
        Dim acl As AuthorizationRuleCollection = pFileInfo.GetAccessControl().GetAccessRules( _
                                                 True, True, GetType(SecurityIdentifier))

        ' Iteriere durch jede enthaltende Rule
        For i As Integer = 0 To acl.Count - 1
            ' FileSystemAccessRule ermitteln
            Dim rule As FileSystemAccessRule = DirectCast(acl(i), FileSystemAccessRule)

            ' Prüfe, ob diese Regel für den aktuellen Benutzer gilt
            If pUser.User.Equals(rule.IdentityReference) Then
                ' Iteriere durch jede Berechtigung
                For Each r As String In [Enum].GetNames(GetType(FileSystemRights))
                    ' Prüfe, ob die Berechtigung in der aktuellen Rule enthalten ist
                    If Contains(r, rule) Then
                        ' Prüfe, ob diese Berechtigung verweigert wird
                        If AccessControlType.Deny = rule.AccessControlType Then
                            ' true -> Eigenschaft des Elementes aus dem Dictionary setzen
                            pRights(r).Deny = True
                        Else
                            ' false -> Eigenschaft des Elementes aus dem Dictionary setzen
                            pRights(r).Allow = True
                        End If
                    End If
                Next
            End If
        Next

        ' Ermittle die Namen aller Gruppen, in denen der Benutzer ist
        Dim groups As IdentityReferenceCollection = pUser.Groups

        ' Iteriere durch jede Gruppe
        For j As Integer = 0 To groups.Count - 1
            ' Iteriere durch jede enthaltende Rule
            For i As Integer = 0 To acl.Count - 1
                ' FileSystemAccessRule ermitteln
                Dim rule As FileSystemAccessRule = DirectCast(acl(i), FileSystemAccessRule)

                ' Prüfe, ob diese Regel für die aktuelle Gruppe gilt
                If groups(j).Equals(rule.IdentityReference) Then
                    ' Iteriere durch jede Berechtigung
                    For Each r As String In [Enum].GetNames(GetType(FileSystemRights))
                        ' Prüfe, ob die Berechtigung in der aktuellen Rule enthalten ist
                        If Contains(r, rule) Then
                            ' Prüfe, ob diese Berechtigung verweigert wird
                            If AccessControlType.Deny = rule.AccessControlType Then
                                ' true -> Eigenschaft des Elementes aus dem Dictionary setzen
                                pRights(r).Deny = True
                            Else
                                ' false -> Eigenschaft des Elementes aus dem Dictionary setzen
                                pRights(r).Allow = True
                            End If
                        End If
                    Next
                End If
            Next
        Next
    End Sub

    ''' <summary>
    ''' Setzt alle Berechtigungen zurück.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ResetRights()
        ' Iteriere durch jeden Eintrag im Dictionary
        For Each pair In pRights
            ' Setze alle Eigenschaften zurück
            Dim ar As AccessRight = pair.Value

            ar.Allow = False
            ar.Deny = False
        Next
    End Sub

#End Region

#Region " Nested Types "

    ''' <summary>
    ''' Stellt die Eigenschaften für die Zugriffsrechte einer Berechtigung bereit.
    ''' </summary>
    ''' <remarks></remarks>
    Private Class AccessRight
        Private pAllow As Boolean
        Private pDeny As Boolean

        ''' <summary>
        ''' Ruft einen Wert ab, der angibt, ob die Berechtigung verweigert wird.
        ''' </summary>
        ''' <value></value>
        ''' <returns>true, wenn die Berechtigung verweigert wird, ansonsten false.</returns>
        ''' <remarks></remarks>
        Public Property Deny() As Boolean
            Get
                Return pDeny
            End Get
            Set(ByVal value As Boolean)
                pDeny = value
            End Set
        End Property

        ''' <summary>
        ''' Ruft einen Wert ab, der angibt, ob die Berechtigung erlaubt wird.
        ''' </summary>
        ''' <value></value>
        ''' <returns>true, wenn die Berechtigung erlaubt wird, ansonsten false.</returns>
        ''' <remarks></remarks>
        Public Property Allow() As Boolean
            Get
                Return pAllow
            End Get
            Set(ByVal value As Boolean)
                pAllow = value
            End Set
        End Property
    End Class

#End Region

End Class
﻿Imports System.Windows.Forms

Public Class Settings


    Private Sub PictureBox1_Click(sender As System.Object, e As System.EventArgs) Handles PictureBox1.Click
        With OpenFileDialog1
            .FileName = "NetIO-GUI.sqlite"
            .Filter = "SQLite Dateien (*.sqlite)|*.sqlite"
            .RestoreDirectory = True
            If .ShowDialog = Windows.Forms.DialogResult.OK Then
                txt_dbfile.Text = .FileName
            End If
        End With
    End Sub

    Private Sub OK_Button_Click(sender As Object, e As EventArgs) Handles OK_Button.Click
        cb_Startmode.Text = cb_Startmode.Text.ToLower
        If cb_Startmode.Text <> "server" And cb_Startmode.Text <> "client" Then
            cb_Startmode.Text = "client"
        End If
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub Cancel_Button_Click(sender As Object, e As EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub LinkLabel1_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked
        Process.Start("https://macvendors.com/register")
    End Sub
End Class

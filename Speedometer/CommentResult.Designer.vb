﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CommentResult
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CommentResult))
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.OK_Button = New System.Windows.Forms.Button()
        Me.Cancel_Button = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txt_Comment = New System.Windows.Forms.TextBox()
        Me.rb_rate1 = New System.Windows.Forms.RadioButton()
        Me.rb_rate2 = New System.Windows.Forms.RadioButton()
        Me.rb_rate3 = New System.Windows.Forms.RadioButton()
        Me.rb_rate4 = New System.Windows.Forms.RadioButton()
        Me.rb_rate5 = New System.Windows.Forms.RadioButton()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        resources.ApplyResources(Me.TableLayoutPanel1, "TableLayoutPanel1")
        Me.TableLayoutPanel1.Controls.Add(Me.OK_Button, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Cancel_Button, 1, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        '
        'OK_Button
        '
        resources.ApplyResources(Me.OK_Button, "OK_Button")
        Me.OK_Button.Name = "OK_Button"
        '
        'Cancel_Button
        '
        resources.ApplyResources(Me.Cancel_Button, "Cancel_Button")
        Me.Cancel_Button.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Cancel_Button.Name = "Cancel_Button"
        '
        'Label3
        '
        resources.ApplyResources(Me.Label3, "Label3")
        Me.Label3.Name = "Label3"
        '
        'txt_Comment
        '
        resources.ApplyResources(Me.txt_Comment, "txt_Comment")
        Me.txt_Comment.Name = "txt_Comment"
        '
        'rb_rate1
        '
        resources.ApplyResources(Me.rb_rate1, "rb_rate1")
        Me.rb_rate1.Name = "rb_rate1"
        Me.rb_rate1.TabStop = True
        Me.rb_rate1.UseVisualStyleBackColor = True
        '
        'rb_rate2
        '
        resources.ApplyResources(Me.rb_rate2, "rb_rate2")
        Me.rb_rate2.Name = "rb_rate2"
        Me.rb_rate2.TabStop = True
        Me.rb_rate2.UseVisualStyleBackColor = True
        '
        'rb_rate3
        '
        resources.ApplyResources(Me.rb_rate3, "rb_rate3")
        Me.rb_rate3.Name = "rb_rate3"
        Me.rb_rate3.TabStop = True
        Me.rb_rate3.UseVisualStyleBackColor = True
        '
        'rb_rate4
        '
        resources.ApplyResources(Me.rb_rate4, "rb_rate4")
        Me.rb_rate4.Name = "rb_rate4"
        Me.rb_rate4.TabStop = True
        Me.rb_rate4.UseVisualStyleBackColor = True
        '
        'rb_rate5
        '
        resources.ApplyResources(Me.rb_rate5, "rb_rate5")
        Me.rb_rate5.Name = "rb_rate5"
        Me.rb_rate5.TabStop = True
        Me.rb_rate5.UseVisualStyleBackColor = True
        '
        'Label4
        '
        resources.ApplyResources(Me.Label4, "Label4")
        Me.Label4.Name = "Label4"
        '
        'Label5
        '
        resources.ApplyResources(Me.Label5, "Label5")
        Me.Label5.Name = "Label5"
        '
        'CommentResult
        '
        Me.AcceptButton = Me.OK_Button
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.Cancel_Button
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.rb_rate5)
        Me.Controls.Add(Me.rb_rate4)
        Me.Controls.Add(Me.rb_rate3)
        Me.Controls.Add(Me.rb_rate2)
        Me.Controls.Add(Me.rb_rate1)
        Me.Controls.Add(Me.txt_Comment)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "CommentResult"
        Me.ShowInTaskbar = False
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents OK_Button As System.Windows.Forms.Button
    Friend WithEvents Cancel_Button As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txt_Comment As System.Windows.Forms.TextBox
    Friend WithEvents rb_rate1 As System.Windows.Forms.RadioButton
    Friend WithEvents rb_rate2 As System.Windows.Forms.RadioButton
    Friend WithEvents rb_rate3 As System.Windows.Forms.RadioButton
    Friend WithEvents rb_rate4 As System.Windows.Forms.RadioButton
    Friend WithEvents rb_rate5 As System.Windows.Forms.RadioButton
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label

End Class

﻿Module ParseCommandline
    Private str_CmdLineArgs As System.Collections.ObjectModel.ReadOnlyCollection(Of String) = My.Application.CommandLineArgs

    Public Function cmd_IsSet(ByVal cmd As String) As Boolean
        cmd_IsSet = False
        For Each arg As String In str_CmdLineArgs
            If arg.StartsWith(cmd) Then
                cmd_IsSet = True
                Exit For
            Else
                cmd_IsSet = False
            End If
        Next
    End Function

    Public Function cmd_ReturnValue(ByVal parm As String) As String
        cmd_ReturnValue = Nothing
        If str_CmdLineArgs.Count = 0 Then
            Return Nothing
            Exit Function
        End If
        For Each arg As String In str_CmdLineArgs
            If arg.Contains("=") Then
                If arg.StartsWith(parm) Then
                    Try
                        Return arg.Split("=")(1)
                    Catch ex As Exception

                    End Try

                End If
            End If
        Next
    End Function
End Module

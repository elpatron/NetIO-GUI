﻿NetIO-GUI
(c) 2012-2018 M. Busche, m.busche@gmail.com

THIS IS FREE SOFTWARE LICENSED UNDER GNU GPL V3 (see gpl.txt)

Download latest Version from https://sourceforge.net/projects/netiogui/

This is Project is still under development (Beta) as of June, 2018.

If you have any issue with NetIO-GUI:
Assure you are using the latest version of NetIO-GUI (see above)
Have a look at the logfile located in %APPDATA\NetIO-GUI\ (installed mode via setup.exe) or in the apllication´s directory (portable mode via portable exe).
If that doesn´t help, please contact the author.

Have a look at whatsnew.[lang].txt for recent changes.

If you like this software and use it on a regular or business basis: please consider to donate (see Help -> Donate)

If you like to contribute to this project (ex. translate NetIO-GUI to a new language): please contact the author.
﻿Imports NLog

Module ServiceControl
    Private logger As Logger = LogManager.GetCurrentClassLogger()

    ''' <summary>
    ''' Startet oder beendet einen Windows Dienst mit net.exe
    ''' </summary>
    ''' <param name="str_servicename"></param>
    ''' <param name="action"></param>
    ''' <returns>Exitcode von net.exe als Integer</returns>
    ''' <remarks>action=true startet Dienst, action=false beendet Dienst</remarks>
    Public Function _controlservice(ByVal str_servicename As String, ByVal action As Boolean) As Integer
        If IsAdmin() = False Then
            _elevate()
        End If
        Dim pr_net As Process = New Process
        ' Dienste .NET 4: Imports System.ServiceProcess
        If action = True Then
            logger.Info("Starting netio service")
            pr_net.StartInfo.Arguments = String.Format("start ""{0}""", str_servicename)
        Else
            logger.Info("Stopping netio service")
            pr_net.StartInfo.Arguments = String.Format("stop ""{0}""", str_servicename)
        End If
        pr_net.StartInfo.UseShellExecute = False
        pr_net.StartInfo.CreateNoWindow = True
        pr_net.StartInfo.FileName = "net.exe"
        Try
            pr_net.Start()
        Catch ex As Exception
            logger.Warn("Error starting/stopping net.exe: " & ex.Message)
            MsgBox("Starten oder Beenden des Dienstes 'NetIO-server' fehlgeschlagen. Fehler: " & ex.Message, MsgBoxStyle.Critical)
        End Try
        pr_net.WaitForExit()
        If pr_net.ExitCode <> 0 Then
            logger.Warn("Failed starting/stopping netio service: " & pr_net.ExitCode)
            MsgBox("Starten oder Beenden des Dienstes 'NetIO-server' fehlgeschlagen. Fehlercode: " & pr_net.ExitCode, MsgBoxStyle.Critical)
        End If
        Return pr_net.ExitCode
        pr_net.Dispose()
    End Function
End Module
